package com.atlassian.bamboo.specs.api.builders.applink;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.applink.ApplicationLinkProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import org.jetbrains.annotations.NotNull;

/**
 * Represents application link between Bamboo and another application, for instance Bitbucket Server.
 */
public class ApplicationLink extends EntityPropertiesBuilder<ApplicationLinkProperties> {
    private String name;
    private String id;

    /**
     * Link to an application by name.
     *
     * @param name name of the application, as shown in Bamboo "Application links" page
     */
    public ApplicationLink name(@NotNull String name) {
        ImporterUtils.checkNotBlank("name", name);
        this.name = name;
        return this;
    }

    /**
     * Link to an application by its application id.
     * <p>
     * Id of the application can be found in application manifest, available at &lt;application_url&gt;/rest/applinks/latest/manifest.
     *
     * @param id of the application
     */
    public ApplicationLink id(@NotNull String id) {
        ImporterUtils.checkNotBlank("id", id);
        this.id = id;
        return this;
    }

    protected ApplicationLinkProperties build() {
        return new ApplicationLinkProperties(name, id);
    }
}
