package com.atlassian.bamboo.specs.api.builders.condition;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import org.jetbrains.annotations.NotNull;

public abstract class TaskCondition<P extends ConditionProperties> extends EntityPropertiesBuilder<P> {

    @NotNull
    protected abstract P build();
}
