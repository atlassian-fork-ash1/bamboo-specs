package com.atlassian.bamboo.specs.api.builders.deployment.configuration;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.deployment.configuration.AnyPluginConfigurationProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class AnyPluginConfiguration extends EnvironmentPluginConfiguration<AnyPluginConfigurationProperties> {

    private AtlassianModuleProperties atlassianPlugin;
    private Map<String, String> configuration = new LinkedHashMap<>();

    /**
     * Specifies a configuration for a specified plugin.
     *
     * @param atlassianPlugin plugin identified by its plugin module key
     * @see AtlassianModule
     */
    public AnyPluginConfiguration(@NotNull final AtlassianModule atlassianPlugin) throws PropertiesValidationException {
        this.atlassianPlugin = EntityPropertiesBuilders.build(atlassianPlugin);
    }

    /**
     * Appends the generic configuration.
     * <p>
     * The configuration should be in the format used by respective plugin. No syntactical nor semantic validation is
     * performed on the source data. The configuration is stored 'as is' in the Bamboo DB.
     */
    public AnyPluginConfiguration configuration(final Map<String, String> configuration) {
        this.configuration.putAll(configuration);
        return this;
    }


    @NotNull
    @Override
    protected AnyPluginConfigurationProperties build() {
        return new AnyPluginConfigurationProperties(atlassianPlugin, configuration);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AnyPluginConfiguration)) {
            return false;
        }
        AnyPluginConfiguration that = (AnyPluginConfiguration) o;
        return Objects.equals(atlassianPlugin, that.atlassianPlugin) &&
                Objects.equals(configuration, that.configuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(atlassianPlugin, configuration);
    }
}
