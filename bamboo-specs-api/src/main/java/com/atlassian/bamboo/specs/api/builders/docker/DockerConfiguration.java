package com.atlassian.bamboo.specs.api.builders.docker;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.deployment.Environment;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.docker.DockerConfigurationProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Configuration of Docker, to be attached to a {@link Job} or an {@link Environment}. With Docker enabled,
 * all eligible {@link Task}s will be executed in a Docker container instead of being processed by the native operating
 * system of the Bamboo agent.
 *
 * @see Job#dockerConfiguration(DockerConfiguration)
 * @see Environment#dockerConfiguration(DockerConfiguration)
 */
public class DockerConfiguration extends EntityPropertiesBuilder<DockerConfigurationProperties> {
    private boolean enabled;
    private String image;
    private boolean defaultVolumes;
    private Map<String, String> volumes;

    /**
     * Creates a new Docker configuration, which will be enabled by default.
     */
    public DockerConfiguration() {
        this.enabled = true;
        this.defaultVolumes = true;
        this.volumes = new LinkedHashMap<>();
    }

    /**
     * Enable or disable Docker. Docker configuration is enabled by default.
     */
    public DockerConfiguration enabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    /**
     * Specify the name of the Docker image to use.
     * <p>
     * You can define the repository host, namespace and tag for the image, by following the Docker image format
     * (e.g. {@code localhost:5000/namespace/image:tag}).
     */
    public DockerConfiguration image(@NotNull String image) {
        ImporterUtils.checkNotBlank("image", image);
        this.image = image;
        return this;
    }

    /**
     * Removes default volumes from the Docker configuration.
     * <p>
     * By default, Bamboo maps some agent directories to corresponding directories in the Docker container. You can
     * disable this behaviour and remove all volumes mounted by default by calling this method.
     * <p>
     * Use {@link #volume(String, String)} to mount custom volumes to the Docker container.
     */
    public DockerConfiguration withoutDefaultVolumes() {
        this.defaultVolumes = false;
        return this;
    }

    /**
     * Add a volume to the Docker configuration.
     * <p>
     * Please note that some volumes are mounted by default. To get rid of default volume mappings, call
     * {@link #withoutDefaultVolumes()}.
     *
     * @param hostDirectory      directory on the Bamboo agent, which will be mounted in the Docker container
     * @param containerDirectory directory in the Docker container, where the volume should be mounted
     */
    public DockerConfiguration volume(@NotNull String hostDirectory, @NotNull String containerDirectory) {
        ImporterUtils.checkNotBlank("hostDirectory", hostDirectory);
        ImporterUtils.checkNotBlank("containerDirectory", containerDirectory);
        if (volumes.containsKey(hostDirectory)) {
            throw new PropertiesValidationException(DockerConfigurationProperties.VALIDATION_CONTEXT,
                    "Host directory " + hostDirectory + " is used by more than one volume.");
        }

        volumes.put(hostDirectory, containerDirectory);
        return this;
    }

    @Override
    protected DockerConfigurationProperties build() {
        final Map<String, String> effectiveVolumes = new LinkedHashMap<>();
        if (enabled && defaultVolumes) {
            effectiveVolumes.putAll(DockerConfigurationProperties.DEFAULT_VOLUMES);
        }
        effectiveVolumes.putAll(volumes);
        return new DockerConfigurationProperties(enabled, image, effectiveVolumes);
    }
}
