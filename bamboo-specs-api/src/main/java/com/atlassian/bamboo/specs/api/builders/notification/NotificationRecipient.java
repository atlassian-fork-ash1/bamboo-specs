package com.atlassian.bamboo.specs.api.builders.notification;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.notification.NotificationRecipientProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents notification recipient.
 */
public abstract class NotificationRecipient<N extends NotificationRecipient<N, P>, P extends NotificationRecipientProperties> extends EntityPropertiesBuilder<P> {
    @NotNull
    @Override
    protected abstract P build();
}
