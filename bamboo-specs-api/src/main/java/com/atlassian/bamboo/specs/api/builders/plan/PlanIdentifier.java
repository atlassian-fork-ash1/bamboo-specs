package com.atlassian.bamboo.specs.api.builders.plan;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

public class PlanIdentifier extends EntityPropertiesBuilder<PlanIdentifierProperties> {
    private BambooKeyProperties projectKey;
    private BambooKeyProperties key;
    private BambooOidProperties oid;

    private PlanIdentifier() {
    }

    public PlanIdentifier(@NotNull String projectKey, @NotNull String planKey) throws PropertiesValidationException {
        checkNotBlank("projectKey", projectKey);
        checkNotBlank("key", planKey);
        this.key = new BambooKeyProperties(planKey);
        this.projectKey = new BambooKeyProperties(projectKey);
    }

    public PlanIdentifier(@NotNull BambooKey projectKey, @NotNull BambooKey planKey) throws PropertiesValidationException {
        checkNotNull("projectKey", projectKey);
        checkNotNull("key", planKey);
        this.key = EntityPropertiesBuilders.build(planKey);
        this.projectKey = EntityPropertiesBuilders.build(projectKey);
    }

    public PlanIdentifier(@NotNull final BambooOid oid) throws PropertiesValidationException {
        checkNotNull("oid", oid);
        this.oid = EntityPropertiesBuilders.build(oid);
    }

    public PlanIdentifier oid(@NotNull final String oid) throws PropertiesValidationException {
        checkNotBlank("oid", oid);
        return oid(new BambooOid(oid));
    }

    public PlanIdentifier oid(@NotNull final BambooOid oid) throws PropertiesValidationException {
        checkNotNull("oid", oid);
        this.oid = EntityPropertiesBuilders.build(oid);
        return this;
    }

    public PlanIdentifier key(@NotNull final String key) throws PropertiesValidationException {
        checkNotBlank("key", key);
        return key(new BambooKey(key));
    }

    public PlanIdentifier key(@NotNull final BambooKey key) throws PropertiesValidationException {
        checkNotNull("key", key);
        this.key = EntityPropertiesBuilders.build(key);
        return this;
    }

    public PlanIdentifier projectKey(@NotNull final String key) throws PropertiesValidationException {
        checkNotNull("key", key);
        return projectKey(new BambooKey(key));
    }

    public PlanIdentifier projectKey(@NotNull final BambooKey key) throws PropertiesValidationException {
        checkNotNull("key", key);
        this.projectKey = EntityPropertiesBuilders.build(key);
        return this;
    }

    protected PlanIdentifierProperties build() throws PropertiesValidationException {
        return new PlanIdentifierProperties(projectKey, key, oid);
    }

    public BambooKey getProjectKey() {
        return (projectKey == null) ? null : new BambooKey(projectKey.getKey());
    }

    public BambooKey getPlanKey() {
        return (key == null) ? null : new BambooKey(key.getKey());
    }

    public BambooOid getPlanOid() {
        return (oid == null) ? null : new BambooOid(oid.getOid());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PlanIdentifier{");
        sb.append("projectKey=").append(projectKey);
        sb.append(", key=").append(key);
        sb.append(", oid=").append(oid);
        sb.append('}');
        return sb.toString();
    }
}
