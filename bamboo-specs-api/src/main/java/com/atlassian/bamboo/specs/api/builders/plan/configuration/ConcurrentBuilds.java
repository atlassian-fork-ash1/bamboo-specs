package com.atlassian.bamboo.specs.api.builders.plan.configuration;

import com.atlassian.bamboo.specs.api.model.plan.configuration.ConcurrentBuildsProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;

public class ConcurrentBuilds extends PluginConfiguration<ConcurrentBuildsProperties> {

    private boolean useSystemWideDefault = true;
    private int maximumNumberOfConcurrentBuilds = 1;

    public ConcurrentBuilds useSystemWideDefault(boolean useSystemWideDefault) {
        this.useSystemWideDefault = useSystemWideDefault;
        return this;
    }

    public ConcurrentBuilds maximumNumberOfConcurrentBuilds(int maximumNumberOfConcurrentBuilds) {
        ImporterUtils.checkThat(ValidationContext.of("maximumNumberOfConcurrentBuilds"), maximumNumberOfConcurrentBuilds > 0, "Maximum number of concurrent builds must be greater than 0.");
        this.maximumNumberOfConcurrentBuilds = maximumNumberOfConcurrentBuilds;
        useSystemWideDefault(false);
        return this;
    }

    @Override
    protected ConcurrentBuildsProperties build() {
        return new ConcurrentBuildsProperties(useSystemWideDefault, maximumNumberOfConcurrentBuilds);
    }
}
