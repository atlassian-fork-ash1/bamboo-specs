/**
 * Custom requirements for a job.
 */
package com.atlassian.bamboo.specs.api.builders.requirement;
