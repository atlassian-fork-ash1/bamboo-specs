/**
 * Generic classes, such as the {@link com.atlassian.bamboo.specs.api.builders.task.AnyTask} class for handling tasks unsupported by Bamboo Specs.
 */
package com.atlassian.bamboo.specs.api.builders.task;
