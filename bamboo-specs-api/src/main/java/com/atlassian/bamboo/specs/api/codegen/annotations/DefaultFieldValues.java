package com.atlassian.bamboo.specs.api.codegen.annotations;

import com.atlassian.bamboo.specs.api.model.EntityProperties;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks method that produces instance of {@link EntityProperties} class that is filled with defaults.
 * Such an object need not to (in in the most case will not) pass the validation.
 * The result of this method is used to determine if field can be skipped during code generation: if value of the
 * field equals the default value defined here, corresponding code is not generated.
 * The fields of the resulting object should be set according to the following rules:
 *  - if field is mandatory, it should be set to null by this method
 *  - if field is optional and skipping respective builder call sets it to value x, it should also
 *  be set to value x by this method.
 * If this annotation is not present, code generator invokes default constructor of the properties class instead.
 * Target method should be private.
 * Target method must not have parameters.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DefaultFieldValues {
}
