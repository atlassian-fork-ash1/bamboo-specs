package com.atlassian.bamboo.specs.api.codegen.annotations;

import com.atlassian.bamboo.specs.api.codegen.Condition;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that a field should be skipped during code generation if condition evaluates to true.
 * This annotation should be added to fields that are derived from other fields.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SkipCodeGenIf {
    Class<? extends Condition> value();
}
