package com.atlassian.bamboo.specs.api.model.credentials;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

@ConstructFrom("name")
@Immutable
public final class SharedCredentialsIdentifierProperties implements EntityProperties {
    private final BambooOidProperties oid;
    private final String name;

    private SharedCredentialsIdentifierProperties() {
        name = null;
        oid = null;
    }

    public SharedCredentialsIdentifierProperties(@Nullable final String name,
                                                 @Nullable final BambooOidProperties oid) throws PropertiesValidationException {
        this.name = name;
        this.oid = oid;

        validate();
    }

    @Nullable
    public String getName() {
        return name;
    }

    public boolean isNameDefined() {
        return StringUtils.isNotBlank(name);
    }

    @Nullable
    public BambooOidProperties getOid() {
        return oid;
    }

    public boolean isOidDefined() {
        return oid != null;
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SharedCredentialsIdentifierProperties that = (SharedCredentialsIdentifierProperties) o;
        return Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getName());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append("oid", oid)
                .append("name", name)
                .build();
    }

    @Override
    public void validate() {
        if (StringUtils.isBlank(name) && oid == null) {
            throw new PropertiesValidationException("Either name or oid need to be defined when referencing shared credentials");
        }
    }
}
