package com.atlassian.bamboo.specs.api.model.credentials;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.SharedCredentialsValidator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
public abstract class SharedCredentialsProperties implements EntityProperties {
    private final String name;
    private final BambooOidProperties oid;

    protected SharedCredentialsProperties() {
        name = null;
        oid = null;
    }

    protected SharedCredentialsProperties(@NotNull final String name,
                                          @Nullable final BambooOidProperties oid) throws PropertiesValidationException {
        this.name = name;
        this.oid = oid;
    }

    @NotNull
    public abstract AtlassianModuleProperties getAtlassianPlugin();

    @NotNull
    public String getName() {
        return name;
    }

    @Nullable
    public BambooOidProperties getOid() {
        return oid;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SharedCredentialsProperties that = (SharedCredentialsProperties) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(getOid(), that.getOid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getOid());
    }

    @Override
    public void validate() {
        checkNotNull("name", name);
        checkNoErrors(SharedCredentialsValidator.validate(this));
    }
}
