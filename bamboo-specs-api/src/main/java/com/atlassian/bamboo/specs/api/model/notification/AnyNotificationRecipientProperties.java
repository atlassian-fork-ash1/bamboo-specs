package com.atlassian.bamboo.specs.api.model.notification;


import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGeneratorName;
import com.atlassian.bamboo.specs.api.codegen.annotations.SkipCodeGen;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@CodeGeneratorName("com.atlassian.bamboo.specs.codegen.emitters.notification.AnyNotificationRecipientEmitter")
@Immutable
public final class AnyNotificationRecipientProperties extends NotificationRecipientProperties {

    private final AtlassianModuleProperties atlassianPlugin;
    private final String recipientString;
    @SkipCodeGen
    private final Set<Applicability> applicableTo;

    private AnyNotificationRecipientProperties() {
        atlassianPlugin = null;
        recipientString = "";
        applicableTo = EnumSet.allOf(Applicability.class);
    }

    public AnyNotificationRecipientProperties(@NotNull final AtlassianModuleProperties atlassianPlugin,
                                              final String recipientString) throws PropertiesValidationException {
        this.atlassianPlugin = atlassianPlugin;
        this.recipientString = recipientString;
        applicableTo = EnumSet.allOf(Applicability.class);
        validate();
    }

    public AnyNotificationRecipientProperties(@NotNull final AtlassianModuleProperties atlassianPlugin,
                                              final String recipientString,
                                              final EnumSet<Applicability> applicableTo) throws PropertiesValidationException {
        this.atlassianPlugin = atlassianPlugin;
        this.recipientString = recipientString;
        this.applicableTo = Collections.unmodifiableSet(new HashSet<>(applicableTo));
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return atlassianPlugin;
    }

    public String getRecipientString() {
        return recipientString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnyNotificationRecipientProperties that = (AnyNotificationRecipientProperties) o;
        return Objects.equals(getAtlassianPlugin(), that.getAtlassianPlugin()) &&
                Objects.equals(getRecipientString(), that.getRecipientString())
                && Objects.equals(applicableTo(), that.applicableTo());
    }

    @Override
    public Set<Applicability> applicableTo() {
        return applicableTo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAtlassianPlugin(), getRecipientString(), applicableTo());
    }

    @Override
    public void validate() {
        checkNotNull("atlassianPlugin", atlassianPlugin);
    }
}
