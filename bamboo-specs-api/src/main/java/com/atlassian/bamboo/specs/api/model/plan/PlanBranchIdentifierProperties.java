package com.atlassian.bamboo.specs.api.model.plan;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;

@ConstructFrom("key")
@Immutable
public final class PlanBranchIdentifierProperties extends AbstractPlanIdentifierProperties {
    private PlanBranchIdentifierProperties() {
    }

    public PlanBranchIdentifierProperties(@Nullable final BambooKeyProperties key,
                                          @Nullable final BambooOidProperties oid) throws PropertiesValidationException {
        super(key, oid);
        validate();
    }

    @Override
    public void validate() {
        if (key == null && oid == null) {
            throw new PropertiesValidationException("Either key or oid need to be defined when referencing plan branch");
        }
    }
}
