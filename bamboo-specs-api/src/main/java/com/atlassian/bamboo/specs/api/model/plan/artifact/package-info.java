/**
 * Artifact definitions and artifact subscriptions.
 */
package com.atlassian.bamboo.specs.api.model.plan.artifact;
