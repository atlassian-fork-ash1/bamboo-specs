package com.atlassian.bamboo.specs.api.model.plan.branches;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.plan.branch.BranchCreationValidator;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;

@Immutable
public final class CreatePlanBranchesProperties implements EntityProperties {

    public static final String DEFAULT_MATCHING_PATTERN = preventInlining(".*");

    public enum Trigger {
        MANUAL, PULL_REQUEST, BRANCH
    }

    private Trigger trigger = Trigger.MANUAL;

    private String matchingPattern;

    private CreatePlanBranchesProperties() {
    }

    public CreatePlanBranchesProperties(final Trigger trigger,
                                        @Nullable final String matchingPattern) throws PropertiesValidationException {
        this.trigger = trigger;
        this.matchingPattern = matchingPattern;

        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CreatePlanBranchesProperties that = (CreatePlanBranchesProperties) o;
        return getTrigger() == that.getTrigger() &&
                Objects.equals(getMatchingPattern(), that.getMatchingPattern());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTrigger(), getMatchingPattern());
    }

    public Trigger getTrigger() {
        return trigger;
    }

    @NotNull
    public String getMatchingPattern() {
        return StringUtils.defaultString(matchingPattern);
    }

    @Override
    public void validate() {
        checkNoErrors(BranchCreationValidator.validate(this));
    }
}
