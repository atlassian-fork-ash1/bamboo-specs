package com.atlassian.bamboo.specs.api.model.repository;

import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.api.validators.repository.VcsRepositoryValidator;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;

@Immutable
public abstract class VcsRepositoryProperties implements EntityProperties {
    private final String name;
    private final String description;
    private final BambooOidProperties oid;

    private final VcsRepositoryViewerProperties repositoryViewerProperties;

    private final String parent;

    protected VcsRepositoryProperties() {
        parent = null;
        oid = null;
        name = null;
        description = null;
        repositoryViewerProperties = null;
    }

    public VcsRepositoryProperties(@Nullable String name,
                                   @Nullable BambooOidProperties oid,
                                   @Nullable String description,
                                   @Nullable String parent,
                                   @Nullable VcsRepositoryViewerProperties repositoryViewerProperties) throws PropertiesValidationException {
        this.name = name;

        this.oid = oid;
        this.description = description;

        this.repositoryViewerProperties = repositoryViewerProperties;

        this.parent = parent;
    }

    @NotNull
    private VcsRepositoryIdentifierProperties toIdentifierUnsafe() throws PropertiesValidationException {
        final VcsRepositoryIdentifier repositoryIdentifier = new VcsRepositoryIdentifier();
        if (!StringUtils.isBlank(name)) {
            repositoryIdentifier.name(name);
        } else if (!StringUtils.isBlank(getParentName())) {
            repositoryIdentifier.name(parent);
        }
        if (oid != null) {
            repositoryIdentifier.oid(oid.getOid());
        }
        return EntityPropertiesBuilders.build(repositoryIdentifier);
    }

    public VcsRepositoryIdentifierProperties toIdentifier() {
        try {
            return toIdentifierUnsafe();
        } catch (PropertiesValidationException e) {
            final String errors = e.getErrors().stream()
                    .map(ValidationProblem::getMessage)
                    .collect(Collectors.joining(", "));
            throw new IllegalStateException(errors);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VcsRepositoryProperties that = (VcsRepositoryProperties) o;
        return Objects.equals(getAtlassianPlugin(), that.getAtlassianPlugin()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getRepositoryViewerProperties(), that.getRepositoryViewerProperties()) &&
                Objects.equals(getParent(), that.getParent());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAtlassianPlugin(), getName(), getDescription(), getOid(),
                getRepositoryViewerProperties(),
                getParent());
    }

    @Nullable
    public abstract AtlassianModuleProperties getAtlassianPlugin();

    @Nullable
    public String getName() {
        return name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    @Nullable
    public BambooOidProperties getOid() {
        return oid;
    }


    @Nullable
    public VcsRepositoryViewerProperties getRepositoryViewerProperties() {
        return repositoryViewerProperties;
    }

    @Nullable
    public String getParent() {
        return parent;
    }

    @Nullable
    public String getParentName() {
        return parent;
    }

    public boolean hasParent() {
        return parent != null;
    }

    @Override
    public void validate() {
        checkNoErrors(VcsRepositoryValidator.validate(this));
        toIdentifierUnsafe(); //identifier should be defined, this will throw if it's not
    }
}
