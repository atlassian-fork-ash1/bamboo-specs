/**
 * <b>The 'validators.*' packages contain data validators, you usually won't call them directly, unless writing own builders.</b>
 */
package com.atlassian.bamboo.specs.api.validators;
