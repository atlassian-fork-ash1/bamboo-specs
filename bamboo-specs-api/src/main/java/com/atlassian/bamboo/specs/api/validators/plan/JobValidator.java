package com.atlassian.bamboo.specs.api.validators.plan;


import com.atlassian.bamboo.specs.api.model.plan.JobProperties;
import com.atlassian.bamboo.specs.api.model.plan.artifact.ArtifactSubscriptionProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class JobValidator {
    private JobValidator() {
    }

    @NotNull
    public static List<ValidationProblem> validate(@NotNull final JobProperties jobProperties) {
        final List<ValidationProblem> errors = new ArrayList<>();

        errors.addAll(validateUniqueSubscriptions(jobProperties.getArtifactSubscriptions()));
        return errors;
    }

    private static Collection<? extends ValidationProblem> validateUniqueSubscriptions(List<ArtifactSubscriptionProperties> artifactSubscriptions) {
        final Set<ArtifactSubscriptionProperties> artifactSubscriptionSet = new HashSet<>();
        final ArrayList<ValidationProblem> errors = new ArrayList<>();

        for (ArtifactSubscriptionProperties artifactSubscription : artifactSubscriptions) {
            if (artifactSubscriptionSet.contains(artifactSubscription)) {
                errors.add(new ValidationProblem(String.format("Duplicate artifact subscription of artifact [%s] to destination [%s]",
                        artifactSubscription.getArtifactName(),
                        artifactSubscription.getDestination())));
            }
            artifactSubscriptionSet.add(artifactSubscription);
        }
        return errors;
    }

}
