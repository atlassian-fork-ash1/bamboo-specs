package com.atlassian.bamboo.specs.api.exceptions;


import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class PropertiesValidationExceptionTest {
    @Test
    public void exceptionReturnsGivenErrors() {
        List<ValidationProblem> errors = new ArrayList<>();
        errors.add(new ValidationProblem("1"));
        errors.add(new ValidationProblem("2"));

        final PropertiesValidationException propertiesValidationException = new PropertiesValidationException(errors);

        assertEquals(propertiesValidationException.getErrors(), errors);
    }

    @Test
    public void exceptionHasMessageContainingErrorsMessages() {
        final ValidationProblem error1 = new ValidationProblem("1");
        final ValidationProblem error2 = new ValidationProblem("2");

        List<ValidationProblem> errors = new ArrayList<>();
        errors.add(error1);
        errors.add(error2);

        final PropertiesValidationException propertiesValidationException = new PropertiesValidationException(errors);

        assertThat(propertiesValidationException.getMessage(), containsString(error1.getMessage()));
        assertThat(propertiesValidationException.getMessage(), containsString(error2.getMessage()));
    }

}