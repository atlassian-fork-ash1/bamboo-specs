package com.atlassian.bamboo.specs.api.model;

import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class BambooOidPropertiesTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void creatingOidPropertiesWithNullOidThrowsException() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new BambooOid(null));
    }

    @Test
    public void oidPropertiesConstructedSuccessfully() throws PropertiesValidationException {
        final String oid = "123abcqz";

        final BambooOidProperties bambooOidProperties = EntityPropertiesBuilders.build(new BambooOid(oid));

        assertEquals(bambooOidProperties.getOid(), oid);
    }

    @Test
    public void creatingOidPropertiesWithInvalidCharactersInOidThrowsException() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new BambooOid("_"));
    }

    @Test
    public void creatingOidPropertiesWithCapitalLettersInOidThrowsException() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new BambooOid("A"));
    }

    @Test
    public void creatingOidPropertiesWithTooLongOidThrowsException() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new BambooOid("1111111111111111111111111111111111111111111111"));
    }
}