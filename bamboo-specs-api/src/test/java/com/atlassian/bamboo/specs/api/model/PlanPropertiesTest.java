package com.atlassian.bamboo.specs.api.model;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.notification.AnyNotificationRecipient;
import com.atlassian.bamboo.specs.api.builders.notification.AnyNotificationType;
import com.atlassian.bamboo.specs.api.builders.notification.EmptyNotificationsList;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryBranch;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.notification.EmptyNotificationsListProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class PlanPropertiesTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldValidateNotifications() {
        final Project project = new Project().key("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .notifications(new EmptyNotificationsList());

        //when
        final PlanProperties properties = EntityPropertiesBuilders.build(plan);

        //then
        Assert.assertThat(properties.getNotifications(), Matchers.hasItem(new EmptyNotificationsListProperties()));
    }

    @Test
    public void shouldRejectListWithEmptyListAndSomeOtherNotification() {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("EmptyNotificationsList must be the only element on the notifications list");
        final Project project = new Project().key("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .notifications(new EmptyNotificationsList(),
                        new Notification()
                                .recipients(new AnyNotificationRecipient(new AtlassianModule("Baz:z")))
                                .type(new AnyNotificationType(new AtlassianModule("Baz:z"))));

        //when
        EntityPropertiesBuilders.build(plan);
    }

    @Test
    public void shouldRejectDoubleBranchOverride() {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Plan: Duplicate branch definition for repository blah");
        final Project project = new Project().key("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .linkedRepositories("blah")
                .repositoryBranches(new VcsRepositoryBranch("blah", "branch1"),
                        new VcsRepositoryBranch("blah", "branch2"));

        //when
        EntityPropertiesBuilders.build(plan);
    }

    @Test
    public void shouldRejectBranchOverrideIfRepoUnknown() {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Plan: Branch defined for unknown repository blah");
        final Project project = new Project().key("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .repositoryBranches(new VcsRepositoryBranch("blah", "branch1"));

        //when
        EntityPropertiesBuilders.build(plan);
    }
}
