package com.atlassian.bamboo.specs.api.model.repository;

import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryBranch;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VcsRepositoryBranchPropertiesTest {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldRejectBranchOverrideWithEmptyBranch() {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Argument branchName can not be blank.");
        VcsRepositoryBranch vcsRepositoryBranch = new VcsRepositoryBranch("blah", "");

        //when
        EntityPropertiesBuilders.build(vcsRepositoryBranch);
    }

    @Test
    public void shouldRejectBranchOverrideWithEmptyRepo() {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Argument repositoryName can not be blank.");
        VcsRepositoryBranch vcsRepositoryBranch = new VcsRepositoryBranch("", "blah");

        //when
        EntityPropertiesBuilders.build(vcsRepositoryBranch);
    }

    @Test
    public void shouldRejectBranchOverrideWithShellInjection() {
        expectedException.expect(PropertiesValidationException.class);
        VcsRepositoryBranch vcsRepositoryBranch = new VcsRepositoryBranch("dsfds", "&blah");

        //when
        EntityPropertiesBuilders.build(vcsRepositoryBranch);
    }

    @Test
    public void shouldRejectBranchOverrideWithXss() {
        expectedException.expect(PropertiesValidationException.class);
        VcsRepositoryBranch vcsRepositoryBranch = new VcsRepositoryBranch("dsfds", "blah").branchDisplayName("<blah>");

        //when
        EntityPropertiesBuilders.build(vcsRepositoryBranch);
    }


}
