# Introduction #

This module contains classes for defining Bamboo configuration as code.

# Documentation #

You can find more on: https://confluence.atlassian.com/display/BAMBOO

# Issue tracker #

You can report bugs and feature requests here: https://jira.atlassian.com/browse/BAM

# License #

Source code in this repository is licensed under the Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0). 

Copyright 2017 Atlassian.

# Contributing #

Feel free to fork this repository and contribute via pull requests. In case this is your first contribution, 
please sign Atlassian Contributor License Agreement:

* [Corporate CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [Individual CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)
