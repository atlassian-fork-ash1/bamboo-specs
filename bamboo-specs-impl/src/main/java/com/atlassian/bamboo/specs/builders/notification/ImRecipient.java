package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.notification.ImRecipientProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents IM account Bamboo can send notifications to.
 */
public class ImRecipient extends NotificationRecipient<ImRecipient, ImRecipientProperties> {
    private String imAddress;

    public ImRecipient(String imAddress) {
        ImporterUtils.checkNotBlank("imAddress", imAddress);
        this.imAddress = imAddress;
    }

    @NotNull
    @Override
    protected ImRecipientProperties build() {
        return new ImRecipientProperties(imAddress);
    }
}
