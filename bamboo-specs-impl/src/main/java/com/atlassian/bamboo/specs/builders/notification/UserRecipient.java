package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.notification.UserRecipientProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents a user Bamboo can send notifications to.
 */
public class UserRecipient extends NotificationRecipient<UserRecipient, UserRecipientProperties> {
    private String userName;

    public UserRecipient(String userName) {
        ImporterUtils.checkNotBlank("userName", userName);
        this.userName = userName;
    }

    @NotNull
    @Override
    protected UserRecipientProperties build() {
        return new UserRecipientProperties(userName);
    }
}
