package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.notification.NotificationType;
import com.atlassian.bamboo.specs.model.notification.XFailedChainsNotificationProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents a notification sent when plan consecutively fails for a specified number of times.
 */
public class XFailedChainsNotification extends NotificationType<XFailedChainsNotification, XFailedChainsNotificationProperties> {
    private int numberOfFailures = 1;

    /**
     * Specifies how many consecutive failures should trigger the notification. Defaults to <em>1</em>.
     */
    public XFailedChainsNotification numberOfFailures(int numberOfFailures) {
        this.numberOfFailures = numberOfFailures;
        return this;
    }

    @NotNull
    @Override
    protected XFailedChainsNotificationProperties build() {
        return new XFailedChainsNotificationProperties(numberOfFailures);
    }
}
