package com.atlassian.bamboo.specs.builders.repository.viewer;

import com.atlassian.bamboo.specs.api.builders.repository.viewer.VcsRepositoryViewer;
import com.atlassian.bamboo.specs.model.repository.viewer.FishEyeRepositoryViewerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a Fisheye repository viewer.
 */
public class FishEyeRepositoryViewer extends VcsRepositoryViewer {
    private String fishEyeUrl;
    private String repositoryName;
    private String repositoryPath;

    /**
     * Specifies a Fisheye viewer.
     */
    public FishEyeRepositoryViewer() {
    }

    /**
     * Sets url of a Fisheye server to be used for link generation.
     */
    public FishEyeRepositoryViewer fishEyeUrl(@NotNull String fishEyeUrl) {
        checkNotNull("fishEyeUrl", fishEyeUrl);
        this.fishEyeUrl = fishEyeUrl;
        return this;
    }

    /**
     * Sets repository name, as defined in Fisheye.
     */
    public FishEyeRepositoryViewer repositoryName(@NotNull String repositoryName) {
        checkNotNull("repositoryName", repositoryName);
        this.repositoryName = repositoryName;
        return this;
    }

    /**
     * Sets a relative path in Fisheye repository. By default, it is empty.
     * <p>
     * When checking out a module from a repository, for example SVN repository, a file path shown in Bamboo is relative to that module and
     * not the repository root. If that's the case, the missing part of the path needs to be defined here.
     */
    public FishEyeRepositoryViewer repositoryPath(@Nullable String repositoryPath) {
        this.repositoryPath = repositoryPath;
        return this;
    }

    @NotNull
    @Override
    protected FishEyeRepositoryViewerProperties build() {
        return new FishEyeRepositoryViewerProperties(fishEyeUrl, repositoryName, repositoryPath);
    }
}
