package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.docker.DockerRegistryTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


/**
 * Class containing common parts to Docker tasks which operates on Docker registry, like {@link DockerPushImageTask} or
 * {@link DockerPullImageTask}.
 */
public abstract class AbstractDockerRegistryTask<T extends AbstractDockerRegistryTask<T>> extends AbstractDockerTask<T, DockerRegistryTaskProperties> {
    @NotNull
    protected String image;
    @NotNull
    protected DockerRegistryTaskProperties.RegistryType registryType;
    @Nullable
    protected String username;
    @Nullable
    protected String password;
    @Nullable
    protected String email;
    @Nullable
    protected SharedCredentialsIdentifierProperties sharedCredentialsIdentifier;

    // Javadocs shall be defined in subclasses.
    public T dockerHubImage(@NotNull String image) {
        this.image = image;
        this.registryType = DockerRegistryTaskProperties.RegistryType.DOCKER_HUB;
        return (T) this;
    }

    // Javadocs shall be defined in subclasses.
    public T customRegistryImage(@NotNull String image) {
        this.image = image;
        this.registryType = DockerRegistryTaskProperties.RegistryType.CUSTOM;
        return (T) this;
    }

    /**
     * Sets authentication settings to authenticate to Docker registry. All fields are required.
     * You can also use default authentication method by {@link #defaultAuthentication()}
     */
    public T authentication(@NotNull String username, @NotNull String password, @NotNull String email) {
        return usernameAuthentication(username, password, email);
    }

    /**
     * Sets authentication settings to authenticate to Docker registry. All fields are required.
     * You can also use default authentication method by {@link #defaultAuthentication()}
     */
    public T authentication(@NotNull String username, @NotNull String password) {
        return usernameAuthentication(username, password, null);
    }

    /**
     * Sets authentication settings to authenticate to Docker registry by Shared credentials.
     */
    public T authentication(@NotNull SharedCredentialsIdentifier sharedCredentialsIdentifier) {
        this.sharedCredentialsIdentifier = EntityPropertiesBuilders.build(sharedCredentialsIdentifier);
        this.username = null;
        this.password = null;
        this.email = null;
        return (T) this;
    }

    /**
     * Use agent's <i>~/.dockercfg</i> credentials to authenticate to Docker registry.
     */
    public T defaultAuthentication() {
        return usernameAuthentication(null, null, null);
    }

    private T usernameAuthentication(@Nullable String username, @Nullable String password, @Nullable String email) {
        this.username = username;
        this.password = password;
        this.email = email;
        sharedCredentialsIdentifier = null;
        return (T) this;
    }
}
