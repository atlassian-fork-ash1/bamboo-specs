package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.ArtifactDownloaderTaskProperties;
import com.atlassian.bamboo.specs.model.task.DownloadItemProperties;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents task that downloads artifacts created by other jobs and plans.
 */
public class ArtifactDownloaderTask extends Task<ArtifactDownloaderTask, ArtifactDownloaderTaskProperties> {
    private PlanIdentifierProperties sourcePlan;
    private List<DownloadItemProperties> artifacts = new ArrayList<>();

    /**
     * Specifies the plan that is the source of the artifacts.
     * If this property is not set, source plan is either the current plan (if the task is used in a job) or the plan
     * associated with the deployment project (if the task is used in a deployment environment).
     */
    public ArtifactDownloaderTask sourcePlan(@NotNull PlanIdentifier planIdentifier) {
        checkNotNull("planIdentifier", planIdentifier);
        this.sourcePlan = EntityPropertiesBuilders.build(planIdentifier);
        return this;
    }

    /**
     * Adds download requests.
     */
    public ArtifactDownloaderTask artifacts(@NotNull DownloadItem... artifacts) {
        checkNotNull("artifacts", artifacts);
        Arrays.stream(artifacts)
                .map(DownloadItem::build)
                .forEach(this.artifacts::add);
        return this;
    }

    @NotNull
    @Override
    protected ArtifactDownloaderTaskProperties build() {
        return new ArtifactDownloaderTaskProperties(description,
                taskEnabled,
                sourcePlan,
                artifacts,
                requirements,
                conditions);
    }
}
