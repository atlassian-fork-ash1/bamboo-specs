package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.task.BowerTaskProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;

/**
 * Represents Bamboo task which executes Bower package manager for Node.js.
 *
 * @see <a href="https://bower.io/">bower.io</a>
 */
public class BowerTask extends BaseNodeTask<BowerTask, BowerTaskProperties> {
    public static final String DEFAULT_BOWER_EXECUTABLE = preventInlining("node_modules/bower/bin/bower");
    public static final String DEFAULT_BOWER_COMMAND = preventInlining("install");

    @NotNull
    private String bowerExecutable = DEFAULT_BOWER_EXECUTABLE;
    @NotNull
    private String command = DEFAULT_BOWER_COMMAND;

    /**
     * Specify path to the Bower executable for this task. Path must be relative to the working directory.
     * <p>
     * Example: {@code node_modules/bower/bin/bower}
     */
    public BowerTask bowerExecutable(@NotNull String bowerExecutable) {
        ImporterUtils.checkNotNull("bowerExecutable", bowerExecutable);
        this.bowerExecutable = bowerExecutable;
        return this;
    }

    /**
     * Command that Bower should run. Defaults to 'install' if not set.
     */
    public BowerTask command(@NotNull String command) {
        ImporterUtils.checkNotNull("command", command);
        this.command = command;
        return this;
    }

    @NotNull
    @Override
    protected BowerTaskProperties build() {
        return new BowerTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                bowerExecutable,
                command,
                requirements,
                conditions);
    }
}
