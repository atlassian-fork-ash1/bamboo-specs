package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.CommandTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotEmpty;

/**
 * Represents a task that executes a command.
 */
public class CommandTask extends Task<CommandTask, CommandTaskProperties> {
    private String executable;
    @Nullable
    private String argument;
    @Nullable
    private String environmentVariables;
    @Nullable
    private String workingSubdirectory;


    /**
     * Sets label (<em>not a path</em>) of command to be executed. This label must be first
     * defined in the GUI on the Administration/Executables page.
     *
     * This field is mandatory
     */
    public CommandTask executable(@NotNull final String executable) {
        checkNotEmpty("executable", executable);
        this.executable = executable;
        return this;
    }

    /**
     * Sets command line argument to be passed when command is executed.
     */
    public CommandTask argument(@NotNull final String argument) {
        checkNotEmpty("argument", argument);
        this.argument = argument;
        return this;
    }

    /**
     * Sets environment variables to be set when command is executed.
     */
    public CommandTask environmentVariables(@NotNull final String environmentVariables) {
        checkNotEmpty("environment variables", environmentVariables);
        this.environmentVariables = environmentVariables;
        return this;
    }

    /**
     * Sets a directory the command should be executed in.
     */
    public CommandTask workingSubdirectory(@NotNull final String workingSubdirectory) {
        checkNotEmpty("working subdirectory", workingSubdirectory);
        this.workingSubdirectory = workingSubdirectory;
        return this;
    }

    @NotNull
    @Override
    protected CommandTaskProperties build() {
        return new CommandTaskProperties(description,
                taskEnabled,
                executable,
                argument,
                environmentVariables,
                workingSubdirectory,
                requirements,
                conditions);
    }
}
