package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.model.task.DownloadItemProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a single download request.
 */
public class DownloadItem extends EntityPropertiesBuilder<DownloadItemProperties> {
    private String artifactName;
    private boolean allArtifacts;
    private String path = "";

    /**
     * Requests downloading all the artifacts provided by the source plan.
     * If specific artifact was selected, the selection is removed.
     */
    public DownloadItem allArtifacts(boolean allArtifacts) {
        this.allArtifacts = allArtifacts;
        this.artifactName = null;
        return this;
    }

    /**
     * Requests downloading a specific artifact.
     * Sets "allArtifacts" to false.
     */
    public DownloadItem artifact(@NotNull String name) {
        checkNotNull("artifact", name);
        this.allArtifacts = false;
        this.artifactName = name;
        return this;
    }

    /**
     * Specifies the path artifact should be downloaded to. The path can be both absolute and relative to the build working directory.
     * Empty by default.
     */
    public DownloadItem path(@NotNull String path) {
        checkNotNull("path", path);
        this.path = path;
        return this;
    }

    protected DownloadItemProperties build() {
        return new DownloadItemProperties(artifactName, allArtifacts, path);
    }
}
