package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.VcsBranchTaskProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Task which creates a new branch in a repository. For
 * <a href="https://www.atlassian.com/git/tutorials/what-is-version-control/">DVCS repositories</a> (which distinguish
 * between local and remote commits) this task will <strong>push</strong> the newly created branch to the remote
 * repository.
 */
public class VcsBranchTask extends BaseVcsTask<VcsBranchTask, VcsBranchTaskProperties> {
    @NotNull
    private String branchName;

    /**
     * Sets the name of the branch to create.
     */
    public VcsBranchTask branchName(@NotNull String branchName) {
        checkNotNull("branchName", branchName);
        this.branchName = branchName;
        return this;
    }

    @NotNull
    @Override
    protected VcsBranchTaskProperties build() {
        return new VcsBranchTaskProperties(
                description,
                taskEnabled,
                requirements,
                conditions,
                defaultRepository,
                repository,
                workingSubdirectory,
                branchName);
    }
}
