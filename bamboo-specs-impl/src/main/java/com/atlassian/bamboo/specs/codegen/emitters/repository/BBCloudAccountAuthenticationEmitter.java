package com.atlassian.bamboo.specs.codegen.emitters.repository;

public class BBCloudAccountAuthenticationEmitter extends AuthenticationEmitter {
    public BBCloudAccountAuthenticationEmitter() {
        super("accountAuthentication");
    }
}
