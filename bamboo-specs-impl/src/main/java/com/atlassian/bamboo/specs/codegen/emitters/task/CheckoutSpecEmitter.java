package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.task.CheckoutItemProperties;
import org.jetbrains.annotations.NotNull;

public class CheckoutSpecEmitter extends EntityPropertiesEmitter<CheckoutItemProperties> {
    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final CheckoutItemProperties entity) throws CodeGenerationException {
        builderClass = CheckoutItem.class;
        fieldsToSkip.add("defaultRepository");
        if (entity.isDefaultRepository()) {
            return emitConstructorInvocation(context, entity) + ".defaultRepository()" + emitFields(context, entity);
        }
        return super.emitCode(context, entity);
    }
}
