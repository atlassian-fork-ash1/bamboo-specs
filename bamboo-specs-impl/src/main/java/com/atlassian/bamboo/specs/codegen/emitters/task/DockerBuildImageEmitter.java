package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.DockerBuildImageTask;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.task.docker.DockerBuildImageTaskProperties;
import org.jetbrains.annotations.NotNull;

public class DockerBuildImageEmitter extends EntityPropertiesEmitter<DockerBuildImageTaskProperties> {

    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final DockerBuildImageTaskProperties entity) throws CodeGenerationException {
        builderClass = DockerBuildImageTask.class;
        fieldsToSkip.add("dockerfileContent");
        fieldsToSkip.add("dockerfile");
        String dockerBuilderCode = super.emitCode(context, entity);

        try {
            context.incIndentation();
            return String.format("%s%s.%s",
                    dockerBuilderCode,
                    context.newLine(),
                    emitDockerContentCode(entity));
        } finally {
            context.decIndentation();
        }
    }

    private String emitDockerContentCode(DockerBuildImageTaskProperties entity) throws CodeGenerationException {
        switch (entity.getDockerfileContent()) {
            case WORKING_DIR:
                return "dockerfileInWorkingDir()";
            case INLINE:
                return String.format("dockerfile(\"%s\")", entity.getDockerfile());

            default:
                throw new CodeGenerationException("Can't handle: " + entity.getDockerfileContent().name());
        }
    }
}
