package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.VcsTagTask;
import com.atlassian.bamboo.specs.model.task.VcsTagTaskProperties;
import org.jetbrains.annotations.NotNull;

public class VcsTagTaskEmitter extends BaseVcsTaskEmitter<VcsTagTaskProperties> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull VcsTagTaskProperties entity) throws CodeGenerationException {
        builderClass = VcsTagTask.class;
        return super.emitCode(context, entity);
    }
}
