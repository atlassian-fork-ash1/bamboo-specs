package com.atlassian.bamboo.specs.codegen.emitters.trigger;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.trigger.ScheduledDeploymentTrigger;
import com.atlassian.bamboo.specs.builders.trigger.ScheduledTrigger;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.trigger.ScheduledTriggerProperties;
import org.jetbrains.annotations.NotNull;

public class ScheduledTriggerEmitter extends EntityPropertiesEmitter<ScheduledTriggerProperties> {
    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final ScheduledTriggerProperties entity) throws CodeGenerationException {
        switch (entity.getContainer()) {
            case PLAN:
                builderClass = ScheduledTrigger.class;
                break;
            case DEPLOYMENT:
                builderClass = ScheduledDeploymentTrigger.class;
                break;
            default:
                throw new IllegalStateException("Unknown type: " + entity.getContainer());
        }
        return super.emitCode(context, entity);
    }
}
