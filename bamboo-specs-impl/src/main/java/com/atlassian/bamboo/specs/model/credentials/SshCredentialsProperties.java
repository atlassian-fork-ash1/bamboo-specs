package com.atlassian.bamboo.specs.model.credentials;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

@Immutable
public final class SshCredentialsProperties extends SharedCredentialsProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugin.sharedCredentials:sshCredentials");

    private final String key;
    private final String passphrase;

    private SshCredentialsProperties() {
        key = null;
        passphrase = null;
    }

    public SshCredentialsProperties(@NotNull final String name,
                                    @Nullable final BambooOidProperties oid,
                                    @NotNull final String key,
                                    @Nullable final String passphrase) throws PropertiesValidationException {
        super(name, oid);
        this.key = key;
        this.passphrase = passphrase;
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @NotNull
    public String getKey() {
        return key;
    }

    @Nullable
    public String getPassphrase() {
        return passphrase;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SshCredentialsProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        SshCredentialsProperties that = (SshCredentialsProperties) o;
        return Objects.equals(key, that.key) &&
                Objects.equals(passphrase, that.passphrase);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), key, passphrase);
    }

    @Override
    public String toString() {
        return "SshCredentialsProperties{" +
                "key='" + key + '\'' +
                ", passphrase='" + passphrase + '\'' +
                '}';
    }

    @Override
    public void validate() {
        super.validate();
    }
}
