package com.atlassian.bamboo.specs.model.credentials;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

@Immutable
public final class UsernamePasswordCredentialsProperties extends SharedCredentialsProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugin.sharedCredentials:usernamePasswordCredentials");

    private final String username;
    private final String password;

    private UsernamePasswordCredentialsProperties() {
        username = null;
        password = null;
    }

    public UsernamePasswordCredentialsProperties(@NotNull final String name,
                                                 @Nullable final BambooOidProperties oid,
                                                 @NotNull final String username,
                                                 @Nullable final String password) throws PropertiesValidationException {
        super(name, oid);
        this.username = username;
        this.password = password;
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UsernamePasswordCredentialsProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        UsernamePasswordCredentialsProperties that = (UsernamePasswordCredentialsProperties) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), username, password);
    }

    @Override
    public void validate() {
        super.validate();
    }
}
