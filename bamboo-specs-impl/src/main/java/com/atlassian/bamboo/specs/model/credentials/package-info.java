/**
 * Shared credentials for user-password and SSH authentication.
 */
package com.atlassian.bamboo.specs.model.credentials;
