/**
 * <b>The 'model.*' packages contain internal data representation of various Bamboo entities, you usually won't use them directly.</b>
 */
package com.atlassian.bamboo.specs.model;
