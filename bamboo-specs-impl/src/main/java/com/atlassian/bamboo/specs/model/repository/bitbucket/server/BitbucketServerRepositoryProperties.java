package com.atlassian.bamboo.specs.model.repository.bitbucket.server;

import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.applink.ApplicationLinkProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsChangeDetectionProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequiredNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsShellInjectionRelatedCharacters;

@Immutable
public final class BitbucketServerRepositoryProperties extends VcsRepositoryProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.stash.atlassian-bamboo-plugin-stash:bbserver");

    //server
    private final ApplicationLinkProperties server;
    private final String projectKey;
    private final String repositorySlug;
    private final String sshPublicKey;
    private final String sshPrivateKey;
    private final BitbucketServerMirrorProperties mirror;
    private final String sshCloneUrl;

    //branch
    private String branch;

    //advanced server opts
    @Setter("shallowClonesEnabled")
    private final boolean useShallowClones;
    @Setter("remoteAgentCacheEnabled")
    private final boolean useRemoteAgentCache;
    @Setter("submodulesEnabled")
    private final boolean useSubmodules;
    private final Duration commandTimeout;
    private final boolean verboseLogs;
    private final boolean fetchWholeRepository;
    @Setter("lfsEnabled")
    private final boolean useLfs;

    //change detection opts
    private VcsChangeDetectionProperties vcsChangeDetection;

    private BitbucketServerRepositoryProperties() {
        server = null;
        projectKey = null;
        repositorySlug = null;
        sshPublicKey = null;
        sshPrivateKey = null;
        branch = "master";
        sshCloneUrl = null;

        useShallowClones = false;
        useRemoteAgentCache = true;
        useSubmodules = false;
        commandTimeout = Duration.ofMinutes(180);
        verboseLogs = false;
        fetchWholeRepository = false;
        useLfs = false;
        mirror = null;
    }

    public BitbucketServerRepositoryProperties(@Nullable final String name,
                                               @Nullable final BambooOidProperties oid,
                                               @Nullable final String description,
                                               @Nullable final String parent,
                                               @Nullable final VcsRepositoryViewerProperties repositoryViewerProperties,
                                               @Nullable final ApplicationLinkProperties server,
                                               @Nullable final String projectKey,
                                               @Nullable final String repositorySlug,
                                               @Nullable final String sshPublicKey,
                                               @Nullable final String sshPrivateKey,
                                               @Nullable final String sshCloneUrl,
                                               @Nullable final String branch,
                                               @Nullable final VcsChangeDetectionProperties vcsChangeDetection,
                                               final boolean useShallowClones,
                                               final boolean useRemoteAgentCache,
                                               final boolean useSubmodules,
                                               @NotNull final Duration commandTimeout,
                                               final boolean verboseLogs,
                                               final boolean fetchWholeRepository,
                                               final boolean useLfs,
                                               @Nullable final BitbucketServerMirrorProperties mirror) throws PropertiesValidationException {
        super(name, oid, description, parent, repositoryViewerProperties);
        this.server = server;
        this.projectKey = projectKey;
        this.repositorySlug = repositorySlug;
        this.sshPublicKey = sshPublicKey;
        this.sshPrivateKey = sshPrivateKey;
        this.branch = branch;
        this.sshCloneUrl = sshCloneUrl;

        this.vcsChangeDetection = vcsChangeDetection;
        this.useRemoteAgentCache = useRemoteAgentCache;
        this.useShallowClones = useShallowClones;
        this.useSubmodules = useSubmodules;
        this.commandTimeout = commandTimeout;
        this.verboseLogs = verboseLogs;
        this.fetchWholeRepository = fetchWholeRepository;
        this.useLfs = useLfs;
        this.mirror = mirror;
        validate();
    }

    @Nullable
    public ApplicationLinkProperties getServer() {
        return server;
    }

    @Nullable
    public String getProjectKey() {
        return projectKey;
    }

    @Nullable
    public String getRepositorySlug() {
        return repositorySlug;
    }

    @Nullable
    public String getSshPublicKey() {
        return sshPublicKey;
    }

    @Nullable
    public String getSshPrivateKey() {
        return sshPrivateKey;
    }

    public String getSshCloneUrl() {
        return sshCloneUrl;
    }

    @Nullable
    public String getBranch() {
        return branch;
    }

    public boolean isUseShallowClones() {
        return useShallowClones;
    }

    public boolean isUseRemoteAgentCache() {
        return useRemoteAgentCache;
    }

    public boolean isUseSubmodules() {
        return useSubmodules;
    }

    @Nullable
    public Duration getCommandTimeout() {
        return commandTimeout;
    }

    public boolean isVerboseLogs() {
        return verboseLogs;
    }

    public boolean isFetchWholeRepository() {
        return fetchWholeRepository;
    }

    public boolean isUseLfs() {
        return useLfs;
    }

    public VcsChangeDetectionProperties getVcsChangeDetection() {
        return vcsChangeDetection;
    }

    public BitbucketServerMirrorProperties getMirror() {
        return mirror;
    }

    @Nullable
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final BitbucketServerRepositoryProperties that = (BitbucketServerRepositoryProperties) o;
        return isUseShallowClones() == that.isUseShallowClones() &&
                isUseRemoteAgentCache() == that.isUseRemoteAgentCache() &&
                isUseSubmodules() == that.isUseSubmodules() &&
                isVerboseLogs() == that.isVerboseLogs() &&
                isFetchWholeRepository() == that.isFetchWholeRepository() &&
                isUseLfs() == that.isUseLfs() &&
                Objects.equals(getServer(), that.getServer()) &&
                Objects.equals(getProjectKey(), that.getProjectKey()) &&
                Objects.equals(getRepositorySlug(), that.getRepositorySlug()) &&
                Objects.equals(getSshPublicKey(), that.getSshPublicKey()) &&
                Objects.equals(getSshPrivateKey(), that.getSshPrivateKey()) &&
                Objects.equals(getBranch(), that.getBranch()) &&
                Objects.equals(getCommandTimeout(), that.getCommandTimeout()) &&
                Objects.equals(getVcsChangeDetection(), that.getVcsChangeDetection()) &&
                Objects.equals(getMirror(), that.getMirror()) &&
                Objects.equals(getSshCloneUrl(), that.getSshCloneUrl());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getServer(), getProjectKey(), getRepositorySlug(), getSshPublicKey(), getSshPrivateKey(),
                getBranch(), isUseShallowClones(), isUseRemoteAgentCache(), isUseSubmodules(),
                getCommandTimeout(), isVerboseLogs(), isFetchWholeRepository(), isUseLfs(), getVcsChangeDetection(), getMirror(), getSshCloneUrl());
    }

    @Override
    public void validate() {
        super.validate();

        final ValidationContext context = ValidationContext.of("Bitbucket Server repository");
        final List<ValidationProblem> errors = new ArrayList<>();

        if (!hasParent() || anyServerPropertyDefined()) {
            checkRequired(context.with("server"), server);
            checkRequiredNotBlank(context.with("projectKey"), projectKey);
            checkRequiredNotBlank(context.with("repositorySlug"), repositorySlug);

            if (sshPublicKey != null || sshPrivateKey != null) {
                //if you define keypair, it must be, well, pair
                checkRequiredNotBlank(context.with("sshPublicKey"), sshPublicKey);
                checkRequiredNotBlank(context.with("sshPrivateKey"), sshPrivateKey);
            }
            if (sshCloneUrl != null) {
                try {
                    final URI uri = new URI(sshCloneUrl);
                    final String scheme = uri.getScheme();
                    if (!"ssh".equals(scheme)) {
                        errors.add(new ValidationProblem(context, "Clone url should be ssh url"));
                    }
                } catch (URISyntaxException e) {
                    errors.add(new ValidationProblem(context, String.format("Malformed URL: %s", sshCloneUrl)));
                }
            }
        }
        if (!hasParent()) {
            if (StringUtils.isBlank(branch)) {
                branch = "master";
            }
        }

        if (branch != null) {
            validateNotContainsShellInjectionRelatedCharacters(context.with("Branch name"), branch)
                    .ifPresent(errors::add);
        }

        if (vcsChangeDetection != null && !vcsChangeDetection.getConfiguration().isEmpty()) {
            errors.add(new ValidationProblem(context.with("Change detection"),
                    "Git repository cannot have any extra change detection configuration."));
        }

        checkNoErrors(errors);
    }

    private boolean anyServerPropertyDefined() {
        return server != null || projectKey != null || repositorySlug != null || sshPublicKey != null || sshPrivateKey != null;
    }
}
