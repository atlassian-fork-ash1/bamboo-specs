package com.atlassian.bamboo.specs.model.repository.git;

import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
public final class SharedCredentialsAuthenticationProperties implements AuthenticationProperties {
    private final SharedCredentialsIdentifierProperties sharedCredentials;

    private SharedCredentialsAuthenticationProperties() {
        sharedCredentials = null;
    }

    public SharedCredentialsAuthenticationProperties(SharedCredentialsIdentifierProperties sharedCredentials) {
        this.sharedCredentials = sharedCredentials;
    }

    public SharedCredentialsIdentifierProperties getSharedCredentials() {
        return sharedCredentials;
    }

    public void validate() {
        checkNotNull("sharedCredentials", sharedCredentials);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SharedCredentialsAuthenticationProperties that = (SharedCredentialsAuthenticationProperties) o;
        return Objects.equals(getSharedCredentials(), that.getSharedCredentials());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSharedCredentials());
    }
}
