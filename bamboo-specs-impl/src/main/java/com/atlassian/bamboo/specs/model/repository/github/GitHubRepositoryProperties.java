package com.atlassian.bamboo.specs.model.repository.github;


import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsChangeDetectionProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.codegen.emitters.repository.GitAuthenticationEmitter;
import com.atlassian.bamboo.specs.model.repository.git.AuthenticationProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.containsBambooVariable;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsShellInjectionRelatedCharacters;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsRelaxedXssRelatedCharacters;


public class GitHubRepositoryProperties extends VcsRepositoryProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-git:gh2");

    private static final Set<String> SUPPORTED_SCHEMES = Stream.of("https", "http").collect(Collectors.toSet());

    private final String repository;
    private String branch;

    @CodeGenerator(GitAuthenticationEmitter.class)
    private final AuthenticationProperties authenticationProperties;

    @Setter("shallowClonesEnabled")
    private final boolean useShallowClones;
    @Setter("remoteAgentCacheEnabled")
    private final boolean useRemoteAgentCache;
    @Setter("submodulesEnabled")
    private final boolean useSubmodules;
    private final Duration commandTimeout;
    private final boolean verboseLogs;
    private final boolean fetchWholeRepository;
    @Setter("lfsEnabled")
    private final boolean useLfs;

    private VcsChangeDetectionProperties vcsChangeDetection;

    private String baseUrl;

    private GitHubRepositoryProperties() {
        repository = null;
        branch = null;
        authenticationProperties = null;
        useShallowClones = false;
        useRemoteAgentCache = true;
        useSubmodules = false;
        commandTimeout = Duration.ofMinutes(180);
        verboseLogs = false;
        fetchWholeRepository = false;
        useLfs = false;
        baseUrl = "https://github.com";
    }

    public GitHubRepositoryProperties(@Nullable final String name,
                                      @Nullable final BambooOidProperties oid,
                                      @Nullable final String description,
                                      @Nullable final String parent,
                                      @Nullable final VcsRepositoryViewerProperties repositoryViewerProperties,
                                      @Nullable final String repository,
                                      @Nullable final String branch,
                                      @Nullable final AuthenticationProperties authenticationProperties,
                                      @Nullable final VcsChangeDetectionProperties vcsChangeDetection,
                                      final boolean useShallowClones,
                                      final boolean useRemoteAgentCache,
                                      final boolean useSubmodules,
                                      @NotNull final Duration commandTimeout,
                                      final boolean verboseLogs,
                                      final boolean fetchWholeRepository,
                                      final boolean useLfs,
                                      final String baseUrl
    ) throws PropertiesValidationException {
        super(name, oid, description, parent, repositoryViewerProperties);

        this.repository = repository;
        this.branch = branch;
        this.authenticationProperties = authenticationProperties;
        this.vcsChangeDetection = vcsChangeDetection;
        this.useRemoteAgentCache = useRemoteAgentCache;
        this.useShallowClones = useShallowClones;
        this.useSubmodules = useSubmodules;
        this.commandTimeout = commandTimeout;
        this.verboseLogs = verboseLogs;
        this.fetchWholeRepository = fetchWholeRepository;
        this.useLfs = useLfs;
        this.baseUrl = baseUrl;

        if (!hasParent()) {
            if (StringUtils.isBlank(this.branch)) {
                this.branch = "master";
            }
        }
        validate();
    }

    public String getRepository() {
        return repository;
    }

    public String getBranch() {
        return branch;
    }

    public AuthenticationProperties getAuthenticationProperties() {
        return authenticationProperties;
    }

    public boolean isUseShallowClones() {
        return useShallowClones;
    }

    public boolean isUseRemoteAgentCache() {
        return useRemoteAgentCache;
    }

    public boolean isUseSubmodules() {
        return useSubmodules;
    }

    public Duration getCommandTimeout() {
        return commandTimeout;
    }

    public boolean isVerboseLogs() {
        return verboseLogs;
    }

    public boolean isFetchWholeRepository() {
        return fetchWholeRepository;
    }

    public boolean isUseLfs() {
        return useLfs;
    }

    public VcsChangeDetectionProperties getVcsChangeDetection() {
        return vcsChangeDetection;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    @Nullable
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        GitHubRepositoryProperties that = (GitHubRepositoryProperties) o;
        return isUseShallowClones() == that.isUseShallowClones() &&
                isUseRemoteAgentCache() == that.isUseRemoteAgentCache() &&
                isUseSubmodules() == that.isUseSubmodules() &&
                isVerboseLogs() == that.isVerboseLogs() &&
                isFetchWholeRepository() == that.isFetchWholeRepository() &&
                isUseLfs() == that.isUseLfs() &&
                Objects.equals(getRepository(), that.getRepository()) &&
                Objects.equals(getBranch(), that.getBranch()) &&
                Objects.equals(getAuthenticationProperties(), that.getAuthenticationProperties()) &&
                Objects.equals(getCommandTimeout(), that.getCommandTimeout()) &&
                Objects.equals(getVcsChangeDetection(), that.getVcsChangeDetection()) &&
                Objects.equals(getBaseUrl(), that.getBaseUrl());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getRepository(), getBranch(), getAuthenticationProperties(), isUseShallowClones(), isUseRemoteAgentCache(), isUseSubmodules(), getCommandTimeout(), isVerboseLogs(), isFetchWholeRepository(), isUseLfs(), getVcsChangeDetection(), getBaseUrl());
    }

    @Override
    public void validate() {
        super.validate();

        final ValidationContext context = ValidationContext.of("GitHub repository");
        final List<ValidationProblem> errors = new ArrayList<>();

        if (!hasParent()) {
            checkRequired(context.with("repository"), repository);
        }

        if (baseUrl != null && !containsBambooVariable(baseUrl)) {
            validateNotContainsRelaxedXssRelatedCharacters(context.with("URL"), baseUrl)
                .ifPresent(errors::add);

            validateNotContainsShellInjectionRelatedCharacters(context.with("URL"), baseUrl)
                .ifPresent(errors::add);

            checkUrl(context.with("URL"))
                .ifPresent(errors::add);
        }

        if (branch != null) {
            validateNotContainsShellInjectionRelatedCharacters(context.with("Branch name"), branch)
                    .ifPresent(errors::add);
        }

        if (vcsChangeDetection != null && !vcsChangeDetection.getConfiguration().isEmpty()) {
            errors.add(new ValidationProblem(context.with("Change detection"),
                    "GitHub repository cannot have any extra change detection configuration."));
        }
        checkNoErrors(errors);
    }

    private Optional<ValidationProblem> checkUrl(@NotNull ValidationContext validationContext) {
        if (baseUrl == null) {
            return Optional.empty();
        }
        if (StringUtils.isBlank(baseUrl)) {
            // we should never get here
            // since we have a default set above
            return Optional.empty();
        }

        try {
            final URI uri = new URI(baseUrl);
            final String scheme = uri.getScheme();
            if (!SUPPORTED_SCHEMES.contains(uri.getScheme())) {
                return Optional.of(new ValidationProblem(validationContext,
                                   "scheme '%s' is not supported - supported schemes are: %s",
                                   scheme, String.join(", ", SUPPORTED_SCHEMES)));
            }
        } catch (URISyntaxException e) {
            return Optional.of(new ValidationProblem(validationContext, String.format("Malformed Base URL: %s", baseUrl)));
        }

        return Optional.empty();

    }
}
