package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.builders.task.BuildWarningParserTask;
import com.atlassian.bamboo.specs.codegen.emitters.task.BuildWarningParserTaskEmitter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@CodeGenerator(BuildWarningParserTaskEmitter.class)
public class BuildWarningParserTaskProperties extends TaskProperties {

    private static final AtlassianModuleProperties MODULE_PROPERTIES =
            new AtlassianModuleProperties("com.atlassian.bamboo.warnings.atlassian-bamboo-warnings:task.warnings.parser");


    private final String parser;
    @Setter("parseFiles")
    @Nullable
    private final String filePattern;
    private final boolean associateWithRepository;
    private final boolean defaultRepository;
    @Nullable
    private final VcsRepositoryIdentifierProperties repository;

    private final boolean failBuild;
    private final int failBuildThreshold;
    private final BuildWarningParserTask.WarningSeverity failBuildSeverity;

    private BuildWarningParserTaskProperties() {
        parser = null;
        filePattern = null;
        associateWithRepository = false;
        defaultRepository = false;
        repository = null;
        failBuild = false;
        failBuildThreshold = 0;
        failBuildSeverity = BuildWarningParserTask.WarningSeverity.LOW;
    }

    public BuildWarningParserTaskProperties(String description,
                                            boolean enabled,
                                            @NotNull List<RequirementProperties> requirements,
                                            @NotNull List<? extends ConditionProperties> conditions,
                                            @NotNull String parser,
                                            @Nullable String filePattern,
                                            boolean associateWithRepository,
                                            boolean defaultRepository,
                                            @Nullable VcsRepositoryIdentifierProperties repository,
                                            boolean failBuild,
                                            int failBuildThreshold,
                                            @NotNull BuildWarningParserTask.WarningSeverity failBuildSeverity) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.parser = parser;
        this.filePattern = filePattern;
        this.associateWithRepository = associateWithRepository;
        this.defaultRepository = defaultRepository;
        this.repository = repository;
        this.failBuild = failBuild;
        this.failBuildThreshold = failBuildThreshold;
        this.failBuildSeverity = failBuildSeverity;
        validate();
    }

    public String getParser() {
        return parser;
    }

    public String getFilePattern() {
        return filePattern;
    }

    public boolean isAssociateWithRepository() {
        return associateWithRepository;
    }

    public boolean isDefaultRepository() {
        return defaultRepository;
    }

    @Nullable
    public VcsRepositoryIdentifierProperties getRepository() {
        return repository;
    }

    public boolean isFailBuild() {
        return failBuild;
    }

    public int getFailBuildThreshold() {
        return failBuildThreshold;
    }

    public BuildWarningParserTask.WarningSeverity getFailBuildSeverity() {
        return failBuildSeverity;
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return MODULE_PROPERTIES;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        BuildWarningParserTaskProperties that = (BuildWarningParserTaskProperties) o;
        return isAssociateWithRepository() == that.isAssociateWithRepository() &&
                isDefaultRepository() == that.isDefaultRepository() &&
                isFailBuild() == that.isFailBuild() &&
                getFailBuildThreshold() == that.getFailBuildThreshold() &&
                Objects.equals(getParser(), that.getParser()) &&
                Objects.equals(getFilePattern(), that.getFilePattern()) &&
                Objects.equals(getRepository(), that.getRepository()) &&
                getFailBuildSeverity() == that.getFailBuildSeverity();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getParser(), getFilePattern(), isAssociateWithRepository(), isDefaultRepository(), getRepository(), isFailBuild(), getFailBuildThreshold(), getFailBuildSeverity());
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("Build warning parser task");
        if (isAssociateWithRepository()) {
            checkThat(context.with("repository"), defaultRepository || repository != null, "Repository to associate with has to be defined");
        }
        if (isFailBuild()) {
            checkThat(context.with("failBuildThreshold"), getFailBuildThreshold() >= 0, "Fail build threshold must not be negative");
            checkThat(context.with("failBuildSeverity"), getFailBuildSeverity() != null, "Relevant severity undefined");
        }
        checkThat(context.with("parser"), StringUtils.isNotBlank(parser), "Parser undefined");
        super.validate();
    }
}
