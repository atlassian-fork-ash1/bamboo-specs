package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.builders.task.GruntTask;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class GruntTaskProperties extends BaseNodeTaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.grunt");
    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Grunt task");

    @NotNull
    private String gruntCliExecutable = GruntTask.DEFAULT_GRUNT_CLI_EXECUTABLE;
    @Nullable
    private String task;
    @Nullable
    private String gruntfile;

    protected GruntTaskProperties() {
        super();
    }

    public GruntTaskProperties(@Nullable String description,
                               boolean enabled,
                               @NotNull String nodeExecutable,
                               @Nullable String environmentVariables,
                               @Nullable String workingSubdirectory,
                               @NotNull String gruntCliExecutable,
                               @Nullable String task,
                               @Nullable String gruntfile,
                               @NotNull List<RequirementProperties> requirements,
                               @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, nodeExecutable, environmentVariables, workingSubdirectory, requirements, conditions);
        this.gruntCliExecutable = gruntCliExecutable;
        this.task = task;
        this.gruntfile = gruntfile;

        validate();
    }

    @Override
    public void validate() {
        super.validate();
        checkThat(getValidationContext(), StringUtils.isNotBlank(gruntCliExecutable), "Grunt CLI executable is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GruntTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final GruntTaskProperties that = (GruntTaskProperties) o;
        return Objects.equals(gruntCliExecutable, that.gruntCliExecutable) &&
                Objects.equals(task, that.task) &&
                Objects.equals(gruntfile, that.gruntfile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), gruntCliExecutable, task, gruntfile);
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    protected ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }

    @NotNull
    public String getGruntCliExecutable() {
        return gruntCliExecutable;
    }

    @Nullable
    public String getTask() {
        return task;
    }

    @Nullable
    public String getGruntfile() {
        return gruntfile;
    }
}
