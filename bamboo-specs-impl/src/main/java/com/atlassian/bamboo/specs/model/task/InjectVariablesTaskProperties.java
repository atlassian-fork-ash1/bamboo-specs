package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class InjectVariablesTaskProperties extends TaskProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-variable-inject-plugin:inject");

    public static final Pattern VALID_NAMESPACE_PATTERN = Pattern.compile("[a-z0-9_\\.]+", Pattern.CASE_INSENSITIVE);
    public static final String DEFAULT_NAMESPACE = preventInlining("inject");

    @NotNull
    private final String path;
    @NotNull
    private final String namespace;
    @NotNull
    private final InjectVariablesScope scope;

    protected InjectVariablesTaskProperties() {
        super();

        path = null;
        namespace = null;
        scope = null;
    }

    public InjectVariablesTaskProperties(@Nullable String description,
                                         boolean enabled,
                                         @NotNull String path,
                                         @NotNull String namespace,
                                         @NotNull InjectVariablesScope scope,
                                         @NotNull List<RequirementProperties> requirements,
                                         @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.path = path;
        this.namespace = namespace;
        this.scope = scope;

        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @NotNull
    public String getPath() {
        return path;
    }

    @NotNull
    public String getNamespace() {
        return namespace;
    }

    @NotNull
    public InjectVariablesScope getScope() {
        return scope;
    }

    @Override
    public void validate() {
        super.validate();
        final ValidationContext context = ValidationContext.of("Inject variables task");
        checkThat(context, StringUtils.isNotBlank(path), "Path is not defined");
        checkThat(context, StringUtils.isNotBlank(namespace), "Namespace is not defined");
        checkThat(context, VALID_NAMESPACE_PATTERN.matcher(namespace).matches(), "Namespace must be alphanumeric");
        checkThat(context, scope != null, "Scope is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InjectVariablesTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        InjectVariablesTaskProperties that = (InjectVariablesTaskProperties) o;
        return Objects.equals(getPath(), that.getPath()) &&
                Objects.equals(getNamespace(), that.getNamespace()) &&
                Objects.equals(getScope(), that.getScope());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getPath(), getNamespace(), getScope());
    }
}
