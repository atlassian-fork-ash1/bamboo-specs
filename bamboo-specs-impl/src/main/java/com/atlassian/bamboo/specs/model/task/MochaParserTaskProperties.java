package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.builders.task.MochaParserTask;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class MochaParserTaskProperties extends TaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.reporter.mocha");
    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Mocha parser task");

    @NotNull
    private String testFilePattern = MochaParserTask.DEFAULT_TEST_FILE_PATTERN;
    @Nullable
    private String workingSubdirectory;
    private boolean pickUpTestResultsCreatedOutsideOfThisBuild;

    protected MochaParserTaskProperties() {
    }

    public MochaParserTaskProperties(@Nullable final String description,
                                     final boolean enabled,
                                     @NotNull final String testFilePattern,
                                     @Nullable final String workingSubdirectory,
                                     final boolean pickUpTestResultsCreatedOutsideOfThisBuild,
                                     final List<RequirementProperties> requirements,
                                     @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.testFilePattern = testFilePattern;
        this.workingSubdirectory = workingSubdirectory;
        this.pickUpTestResultsCreatedOutsideOfThisBuild = pickUpTestResultsCreatedOutsideOfThisBuild;

        validate();
    }

    @Override
    public void validate() {
        super.validate();
        checkThat(getValidationContext(), StringUtils.isNotBlank(testFilePattern), "Test file pattern is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MochaParserTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final MochaParserTaskProperties that = (MochaParserTaskProperties) o;
        return isPickUpTestResultsCreatedOutsideOfThisBuild() == that.isPickUpTestResultsCreatedOutsideOfThisBuild() &&
                Objects.equals(getTestFilePattern(), that.getTestFilePattern()) &&
                Objects.equals(getWorkingSubdirectory(), that.getWorkingSubdirectory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getTestFilePattern(), getWorkingSubdirectory(), isPickUpTestResultsCreatedOutsideOfThisBuild());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    private ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }

    @NotNull
    public String getTestFilePattern() {
        return testFilePattern;
    }

    @Nullable
    public String getWorkingSubdirectory() {
        return workingSubdirectory;
    }

    public boolean isPickUpTestResultsCreatedOutsideOfThisBuild() {
        return pickUpTestResultsCreatedOutsideOfThisBuild;
    }

    @Override
    public EnumSet<Applicability> applicableTo() {
        return EnumSet.of(Applicability.PLANS);
    }

}
