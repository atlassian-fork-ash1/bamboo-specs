package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.builders.task.MochaRunnerTask;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class MochaRunnerTaskProperties extends BaseNodeTaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.mocha");
    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Mocha runner task");

    @NotNull
    private String mochaExecutable = MochaRunnerTask.DEFAULT_MOCHA_EXECUTABLE;
    @NotNull
    private String testFilesAndDirectories = MochaRunnerTask.DEFAULT_TEST_DIRECTORY;
    private boolean parseTestResults = MochaRunnerTask.DEFAULT_PARSE_TEST_RESULTS;
    @Nullable
    private String arguments;

    protected MochaRunnerTaskProperties() {
        super();
    }

    public MochaRunnerTaskProperties(@Nullable String description,
                                     boolean enabled,
                                     @NotNull String nodeExecutable,
                                     @Nullable String environmentVariables,
                                     @Nullable String workingSubdirectory,
                                     @NotNull String mochaExecutable,
                                     @NotNull String testFilesAndDirectories,
                                     boolean parseTestResults,
                                     @Nullable String arguments,
                                     @NotNull List<RequirementProperties> requirements,
                                     @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, nodeExecutable, environmentVariables, workingSubdirectory, requirements, conditions);
        this.mochaExecutable = mochaExecutable;
        this.testFilesAndDirectories = testFilesAndDirectories;
        this.parseTestResults = parseTestResults;
        this.arguments = arguments;

        validate();
    }

    @Override
    public void validate() {
        super.validate();
        checkThat(getValidationContext(), StringUtils.isNotBlank(mochaExecutable),
                "Mocha executable is not defined");
        checkThat(getValidationContext(), StringUtils.isNotBlank(testFilesAndDirectories),
                "Files and/or directories containing Mocha tests are not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MochaRunnerTaskProperties)) return false;
        if (!super.equals(o)) return false;
        MochaRunnerTaskProperties that = (MochaRunnerTaskProperties) o;
        return isParseTestResults() == that.isParseTestResults() &&
                Objects.equals(getMochaExecutable(), that.getMochaExecutable()) &&
                Objects.equals(getTestFilesAndDirectories(), that.getTestFilesAndDirectories()) &&
                Objects.equals(getArguments(), that.getArguments());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getMochaExecutable(), getTestFilesAndDirectories(), isParseTestResults(), getArguments());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    protected ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }

    @NotNull
    public String getMochaExecutable() {
        return mochaExecutable;
    }

    @NotNull
    public String getTestFilesAndDirectories() {
        return testFilesAndDirectories;
    }

    public boolean isParseTestResults() {
        return parseTestResults;
    }

    @Nullable
    public String getArguments() {
        return arguments;
    }
}
