package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.codegen.emitters.task.VcsBranchTaskEmitter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequiredNotBlank;

@CodeGenerator(VcsBranchTaskEmitter.class)
public final class VcsBranchTaskProperties extends BaseVcsTaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.vcs:task.vcs.branching");
    private static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("VCS Branch Task");

    @NotNull
    private final String branchName;

    private VcsBranchTaskProperties() {
        super();
        this.branchName = null;
    }

    public VcsBranchTaskProperties(@Nullable String description,
                                   boolean enabled,
                                   @NotNull List<RequirementProperties> requirements,
                                   @NotNull List<? extends ConditionProperties> conditions,
                                   boolean defaultRepository,
                                   @Nullable VcsRepositoryIdentifierProperties repository,
                                   @Nullable String workingSubdirectory,
                                   @NotNull String branchName) {
        super(description, enabled, requirements, conditions, defaultRepository, repository, workingSubdirectory);
        this.branchName = branchName;
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @NotNull
    @Override
    protected ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }

    @Override
    public void validate() {
        super.validate();
        checkRequiredNotBlank(getValidationContext().with("branch name"), branchName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VcsBranchTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final VcsBranchTaskProperties that = (VcsBranchTaskProperties) o;
        return Objects.equals(branchName, that.branchName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), branchName);
    }

    @NotNull
    public String getBranchName() {
        return branchName;
    }
}
