package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.codegen.emitters.task.VcsTagTaskEmitter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequiredNotBlank;

@CodeGenerator(VcsTagTaskEmitter.class)
public final class VcsTagTaskProperties extends BaseVcsTaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.vcs:task.vcs.tagging");
    private static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("VCS Tag Task");

    @NotNull
    private final String tagName;

    private VcsTagTaskProperties() {
        super();
        this.tagName = null;
    }

    public VcsTagTaskProperties(@Nullable String description,
                                boolean enabled,
                                @NotNull List<RequirementProperties> requirements,
                                @NotNull List<? extends ConditionProperties> conditions,
                                boolean defaultRepository,
                                @Nullable VcsRepositoryIdentifierProperties repository,
                                @Nullable String workingSubdirectory,
                                @NotNull String tagName) {
        super(description, enabled, requirements, conditions, defaultRepository, repository, workingSubdirectory);
        this.tagName = tagName;
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @NotNull
    @Override
    protected ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }

    @Override
    public void validate() {
        super.validate();
        checkRequiredNotBlank(getValidationContext().with("tag name"), tagName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VcsTagTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final VcsTagTaskProperties that = (VcsTagTaskProperties) o;
        return Objects.equals(tagName, that.tagName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), tagName);
    }

    @NotNull
    public String getTagName() {
        return tagName;
    }
}
