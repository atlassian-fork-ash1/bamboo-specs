package com.atlassian.bamboo.specs.util;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;

import java.io.Writer;

/**
 * Wrapper to convert bamboo spec object to String.
 */
public final class BambooSpecSerializer {
    private BambooSpecSerializer() {
    }

    public static <T extends EntityProperties> String dump(final EntityPropertiesBuilder<T> entity) {
        final T entityProperties = EntityPropertiesBuilders.build(entity);
        final BambooSpecProperties bambooSpecProperties = new BambooSpecProperties(entityProperties);
        return IsolatedYamlizator.execute(bambooSpecProperties);
    }

    public static <T extends EntityProperties> void dump(final EntityPropertiesBuilder<T> entity, final Writer writer) {
        final T entityProperties = EntityPropertiesBuilders.build(entity);
        final BambooSpecProperties bambooSpecProperties = new BambooSpecProperties(entityProperties);
        Yamlizator.getYaml().dump(bambooSpecProperties, writer);
    }
}
