package com.atlassian.bamboo.specs.util;

import org.yaml.snakeyaml.constructor.Construct;
import org.yaml.snakeyaml.representer.Represent;

public final class CustomYamlers {
    public static final CustomYamler[] YAMLERS = {
            DurationYamler.INSTANCE
    };

    private CustomYamlers() {
    }

    public interface CustomYamler {
        Construct getConstructor();

        Class<?> getYamledClass();

        Represent getRepresenter();
    }
}
