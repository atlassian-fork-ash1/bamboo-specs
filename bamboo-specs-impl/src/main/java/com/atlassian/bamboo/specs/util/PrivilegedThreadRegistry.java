package com.atlassian.bamboo.specs.util;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * Used by specs runner to get a reference to the yamlizator thread.
 */
public enum PrivilegedThreadRegistry implements Supplier<Thread> {
    INSTANCE;

    @Override
    public Thread get() {
        return Objects.requireNonNull(IsolatedYamlizator.getThread());
    }
}
