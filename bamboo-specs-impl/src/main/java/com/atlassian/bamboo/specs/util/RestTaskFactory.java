package com.atlassian.bamboo.specs.util;

import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentials;
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment;
import com.atlassian.bamboo.specs.api.builders.permission.DeploymentPermissions;
import com.atlassian.bamboo.specs.api.builders.permission.EnvironmentPermissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepository;

import java.net.URI;

public final class RestTaskFactory {
    public static final class RestTask {
        private final URI restEntpointUri;
        private final UserPasswordCredentials userPasswordCredentials;
        private final String humanReadableId;
        private final String yamlString;

        private RestTask(final URI restEntpointUri,
                         final UserPasswordCredentials userPasswordCredentials,
                         final String humanReadableId,
                         final String yamlString) {
            this.restEntpointUri = restEntpointUri;
            this.userPasswordCredentials = userPasswordCredentials;
            this.humanReadableId = humanReadableId;
            this.yamlString = yamlString;
        }

        public URI getRestEntpointUri() {
            return restEntpointUri;
        }

        public UserPasswordCredentials getUserPasswordCredentials() {
            return userPasswordCredentials;
        }

        public String getHumanReadableId() {
            return humanReadableId;
        }

        public String getYamlString() {
            return yamlString;
        }
    }

    private RestTaskFactory() {
    }

    public static RestTask create(final URI bambooServerUrl,
                                  final UserPasswordCredentials userPasswordCredentials,
                                  final RootEntityPropertiesBuilder entity, final String payload) {
        final String entityApiUrl = getApiUrl(entity);
        final URI restEntpointUri = bambooServerUrl.resolve(entityApiUrl);

        final String humanReadableId = entity.humanReadableId();
        return new RestTask(restEntpointUri, userPasswordCredentials, humanReadableId, payload);
    }

    private static String getApiUrl(final RootEntityPropertiesBuilder entity) {
        // If you add any new API here, please make sure you add also the Specs version validation on Bamboo side.
        // look at it.com.atlassian.bamboo.plugins.rest.configuration.external.BambooSpecsVersionValidationTest.
        if (entity instanceof Plan) {
            return "rest/api/latest/import/plan";
        } else if (entity instanceof Deployment) {
            return "rest/api/latest/import/deployment";
        } else if (entity instanceof VcsRepository) {
            return "rest/api/latest/import/repository";
        } else if (entity instanceof SharedCredentials) {
            return "rest/api/latest/import/sharedCredentials";
        } else if (entity instanceof PlanPermissions) {
            return "rest/api/latest/import/plan/permission";
        } else if (entity instanceof DeploymentPermissions) {
            return "rest/api/latest/import/deployment/permission";
        } else if (entity instanceof EnvironmentPermissions) {
            return "rest/api/latest/import/deployment/environment/permission";
        } else {
            throw new IllegalArgumentException("Unknown entity " + entity.getClass());
        }

    }
}
