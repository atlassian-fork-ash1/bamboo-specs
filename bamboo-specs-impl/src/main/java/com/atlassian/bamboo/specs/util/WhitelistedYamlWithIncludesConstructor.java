package com.atlassian.bamboo.specs.util;

import org.yaml.snakeyaml.nodes.Tag;

import java.nio.file.Path;

class WhitelistedYamlWithIncludesConstructor extends WhitelistedYamlConstructor {

    static final String INCLUDE_TAG = "!include";

    WhitelistedYamlWithIncludesConstructor(final int maxDepth, final int depth, final Path parentPath) {
        this.yamlConstructors.put(new Tag(INCLUDE_TAG), new IncludeTag(maxDepth, depth, parentPath));
    }
}
