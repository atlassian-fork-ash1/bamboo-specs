package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.BasicTaskConfigProvider;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.model.task.BuildWarningParserTaskProperties;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class BuildWarningParserTaskTest {
    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithDefaultRepository(boolean enabled, String description) {
        assertEquals(
                new BuildWarningParserTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        "Clang (LLVM)",
                        null,
                        true,
                        true,
                        null,
                        true,
                        1,
                        BuildWarningParserTask.WarningSeverity.LOW
                ),
                new BuildWarningParserTask()
                        .description(description)
                        .enabled(enabled)
                        .defaultRepository()
                        .parseLogs()
                        .parser("Clang (LLVM)")
                        .defaultRepository()
                        .failBuild(true)
                        .failBuildThreshold(1)
                        .build());
    }

    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithRepositoryName(boolean enabled, String description) {
        assertEquals(
                new BuildWarningParserTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        "Clang (LLVM)",
                        null,
                        true,
                        false,
                        new VcsRepositoryIdentifierProperties("bamboo-node-plugin", null),
                        true,
                        1,
                        BuildWarningParserTask.WarningSeverity.LOW),
                new BuildWarningParserTask()
                        .description(description)
                        .enabled(enabled)
                        .parseLogs()
                        .parser("Clang (LLVM)")
                        .repository("bamboo-node-plugin")
                        .failBuild(true)
                        .failBuildThreshold(1)
                        .build());
    }

    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithRepositoryIdentifier(boolean enabled, String description) {
        assertEquals(
                new BuildWarningParserTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        "Clang (LLVM)",
                        null,
                        true,
                        false,
                        new VcsRepositoryIdentifierProperties(null, new BambooOidProperties("123456789")),
                        true,
                        1,
                        BuildWarningParserTask.WarningSeverity.LOW),
                new BuildWarningParserTask()
                        .description(description)
                        .enabled(enabled)
                        .parseLogs()
                        .parser("Clang (LLVM)")
                        .repository(new VcsRepositoryIdentifier().oid("123456789"))
                        .failBuild(true)
                        .failBuildThreshold(1)
                        .build());
    }

    @Test(expected = PropertiesValidationException.class)
    @Parameters(source = BasicTaskConfigProvider.class)
    public void repositoryIsRequired(boolean enabled, String description) {
        new BuildWarningParserTaskProperties(
                description,
                enabled,
                Collections.emptyList(),
                Collections.emptyList(),
                "Clang (LLVM)",
                null,
                true,
                false,
                null,
                true,
                1,
                BuildWarningParserTask.WarningSeverity.LOW);
    }

    @Test(expected = PropertiesValidationException.class)
    @Parameters(source = BasicTaskConfigProvider.class)
    public void testParserRequired(boolean enabled, String description) {
        new BuildWarningParserTask()
                .description(description)
                .enabled(enabled)
                .parseLogs()
                .failBuild(true)
                .failBuildThreshold(1)
                .build();
    }
}
