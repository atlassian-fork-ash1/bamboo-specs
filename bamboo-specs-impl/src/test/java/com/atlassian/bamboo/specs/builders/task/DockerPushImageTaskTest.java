package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.docker.DockerRegistryTaskProperties;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class DockerPushImageTaskTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final String IMAGE_NAME = "registry.address:port/some/namespace/image:latest";
    private static final String ENVIRONMENT_VARIABLES = "JAVA_HOME=/opt/java6";
    private static final String SUBDIRECTORY = "project/";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";

    @Test
    public void testMinimal() {
        DockerRegistryTaskProperties properties = new DockerPushImageTask()
                .dockerHubImage(IMAGE_NAME)
                .defaultAuthentication()
                .build();

        assertThat(properties.getOperationType(), is(DockerRegistryTaskProperties.OperationType.PUSH));
        assertThat(properties.getRegistryType(), is(DockerRegistryTaskProperties.RegistryType.DOCKER_HUB));
        assertThat(properties.getImage(), equalTo(IMAGE_NAME));
        assertThat(properties.getUsername(), isEmptyOrNullString());
        assertThat(properties.getPassword(), isEmptyOrNullString());
        assertThat(properties.getEnvironmentVariables(), isEmptyOrNullString());
        assertThat(properties.getWorkingSubdirectory(), isEmptyOrNullString());
    }

    @Test
    public void testFull() {
        DockerRegistryTaskProperties properties = new DockerPushImageTask()
                .customRegistryImage(IMAGE_NAME)
                .authentication(USERNAME, PASSWORD, EMAIL)
                .environmentVariables(ENVIRONMENT_VARIABLES)
                .workingSubdirectory(SUBDIRECTORY)
                .build();

        assertThat(properties.getOperationType(), is(DockerRegistryTaskProperties.OperationType.PUSH));
        assertThat(properties.getRegistryType(), is(DockerRegistryTaskProperties.RegistryType.CUSTOM));
        assertThat(properties.getImage(), equalTo(IMAGE_NAME));
        assertThat(properties.getUsername(), equalTo(USERNAME));
        assertThat(properties.getPassword(), equalTo(PASSWORD));
        assertThat(properties.getEmail(), equalTo(EMAIL));
        assertThat(properties.getEnvironmentVariables(), equalTo(ENVIRONMENT_VARIABLES));
        assertThat(properties.getWorkingSubdirectory(), equalTo(SUBDIRECTORY));
    }

    @Test
    public void testSharedCredentialsAuthorization() {
        final String sharedCredentialsName = "docker";
        DockerRegistryTaskProperties properties = new DockerPushImageTask()
                .customRegistryImage(IMAGE_NAME)
                .authentication(new SharedCredentialsIdentifier(sharedCredentialsName))
                .environmentVariables(ENVIRONMENT_VARIABLES)
                .workingSubdirectory(SUBDIRECTORY)
                .build();

        assertThat(properties.getSharedCredentialsIdentifier().getName(), equalTo(sharedCredentialsName));
    }


    @Test()
    public void testValidateNoImageName() {
        expectedException.expect(PropertiesValidationException.class);

        DockerRegistryTaskProperties properties = new DockerPushImageTask()
                .dockerHubImage("")
                .build();

    }

    @Parameters(method = "authenticationParameters")
    @Test
    public void testValidationWrongAuthentication(String username, String password, String email, Boolean expectFail) throws Exception {
        if (expectFail) {
            expectedException.expect(PropertiesValidationException.class);
        }

        new DockerPushImageTask()
                .dockerHubImage(IMAGE_NAME)
                .authentication(username, password, email)
                .build();
    }

    public Object[] authenticationParameters() {
        return new Object[]{
                new Object[]{
                        USERNAME, "", "", true
                },
                new Object[]{
                        "", PASSWORD, "", true
                },
                new Object[]{
                        "", "", EMAIL, false
                }
        };
    }
}