package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.MochaParserTaskProperties;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MochaParserTaskTest {
    @Test(expected = PropertiesValidationException.class)
    public void testTestFilePatternIsRequired() {
        new MochaParserTask()
                .testFilePattern("")
                .build();
    }

    @Test
    public void testMinimalConfigurationWorks() {
        // should not throw exception
        new MochaParserTask()
                .build();
    }

    @Test
    public void testAllParamsAreSetWhenBuildingProperties() {
        final String description = "execute Gulp";
        final boolean taskEnabled = false;
        final String workingSubdirectory = "plugin";
        final String testFilePattern = "mocha-result.json";
        final boolean pickUpTestResultsCreatedOutsideOfThisBuild = true;

        final MochaParserTask MochaParserTask = new MochaParserTask()
                .description(description)
                .enabled(taskEnabled)
                .testFilePattern(testFilePattern)
                .pickUpTestResultsCreatedOutsideOfThisBuild(pickUpTestResultsCreatedOutsideOfThisBuild)
                .workingSubdirectory(workingSubdirectory);

        final MochaParserTaskProperties expectedProperties = new MochaParserTaskProperties(
                description,
                taskEnabled,
                testFilePattern,
                workingSubdirectory,
                pickUpTestResultsCreatedOutsideOfThisBuild,
                Collections.emptyList(),
                Collections.emptyList());

        assertThat(MochaParserTask.build(), is(equalTo(expectedProperties)));
    }
}
