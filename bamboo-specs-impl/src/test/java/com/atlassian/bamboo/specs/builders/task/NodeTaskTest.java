package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.NodeTaskProperties;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class NodeTaskTest {
    @Test(expected = PropertiesValidationException.class)
    public void testNodeExecutableIsRequired() {
        new NodeTask()
                .script("application.js")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testScriptIsRequired() {
        new NodeTask()
                .nodeExecutable("Node.js 4.2")
                .build();
    }

    @Test
    public void testMinimalConfigurationWorks() {
        // should not throw exception
        new NodeTask()
                .nodeExecutable("Node.js 6")
                .script("node_modules/my-application/start.js")
                .build();
    }

    @Test
    public void testAllParamsAreSetWhenBuildingProperties() {
        final String description = "execute Gulp";
        final boolean taskEnabled = true;
        final String nodeExecutable = "Node.js 4.2";
        final String script = "server.js";
        final String arguments = "--port 6990 --contextPath /app";
        final String environmentVariables = "NODE_HOME=/tmp/home";
        final String workingSubdirectory = "plugin";

        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        final NodeTask nodeTask = new NodeTask()
                .description(description)
                .enabled(taskEnabled)
                .nodeExecutable(nodeExecutable)
                .script(script)
                .arguments(arguments)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory)
                .requirements(requirement);

        final NodeTaskProperties expectedProperties = new NodeTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                script,
                arguments,
                Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                Collections.emptyList());

        assertThat(nodeTask.build(), is(equalTo(expectedProperties)));
    }
}
