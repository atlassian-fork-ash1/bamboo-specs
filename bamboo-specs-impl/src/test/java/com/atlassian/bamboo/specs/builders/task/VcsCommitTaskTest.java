package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.BasicTaskConfigProvider;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.model.task.VcsCommitTaskProperties;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class VcsCommitTaskTest {
    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithDefaultRepository(boolean enabled, String description) {
        assertEquals(
                new VcsCommitTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        true,
                        null,
                        null,
                        "hello, world!"),
                new VcsCommitTask()
                        .description(description)
                        .enabled(enabled)
                        .defaultRepository()
                        .commitMessage("hello, world!")
                        .build());
    }

    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithRepositoryName(boolean enabled, String description) {
        assertEquals(
                new VcsCommitTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        false,
                        new VcsRepositoryIdentifierProperties("bamboo-node-plugin", null),
                        null,
                        "hello, world!"),
                new VcsCommitTask()
                        .description(description)
                        .enabled(enabled)
                        .repository("bamboo-node-plugin")
                        .commitMessage("hello, world!")
                        .build());
    }

    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithRepositoryIdentifier(boolean enabled, String description) {
        assertEquals(
                new VcsCommitTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        false,
                        new VcsRepositoryIdentifierProperties(null, new BambooOidProperties("123456789")),
                        null,
                        "hello, world!"),
                new VcsCommitTask()
                        .description(description)
                        .enabled(enabled)
                        .repository(new VcsRepositoryIdentifier().oid("123456789"))
                        .commitMessage("hello, world!")
                        .build());
    }

    @Test(expected = PropertiesValidationException.class)
    @Parameters(source = BasicTaskConfigProvider.class)
    public void repositoryIsRequired(boolean enabled, String description) {
        new VcsCommitTask()
                .description(description)
                .enabled(enabled)
                .commitMessage("hello, world!")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    @Parameters(source = BasicTaskConfigProvider.class)
    public void commitMessageIsRequired(boolean enabled, String description) {
        new VcsCommitTask()
                .description(description)
                .enabled(enabled)
                .defaultRepository()
                .build();
    }
}
