package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.BasicTaskConfigProvider;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.model.task.VcsTagTaskProperties;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class VcsTagTaskTest {
    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithDefaultRepository(boolean enabled, String description) {
        assertEquals(
                new VcsTagTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        true,
                        null,
                        null,
                        "tag-15"),
                new VcsTagTask()
                        .description(description)
                        .enabled(enabled)
                        .defaultRepository()
                        .tagName("tag-15")
                        .build());
    }

    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithRepositoryName(boolean enabled, String description) {
        assertEquals(
                new VcsTagTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        false,
                        new VcsRepositoryIdentifierProperties("bamboo-node-plugin", null),
                        null,
                        "tag-15"),
                new VcsTagTask()
                        .description(description)
                        .enabled(enabled)
                        .repository("bamboo-node-plugin")
                        .tagName("tag-15")
                        .build());
    }

    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithRepositoryIdentifier(boolean enabled, String description) {
        assertEquals(
                new VcsTagTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        false,
                        new VcsRepositoryIdentifierProperties(null, new BambooOidProperties("123456789")),
                        null,
                        "tag-15"),
                new VcsTagTask()
                        .description(description)
                        .enabled(enabled)
                        .repository(new VcsRepositoryIdentifier().oid("123456789"))
                        .tagName("tag-15")
                        .build());
    }

    @Test(expected = PropertiesValidationException.class)
    @Parameters(source = BasicTaskConfigProvider.class)
    public void repositoryIsRequired(boolean enabled, String description) {
        new VcsTagTask()
                .description(description)
                .enabled(enabled)
                .tagName("tag-15")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    @Parameters(source = BasicTaskConfigProvider.class)
    public void tagNameIsRequired(boolean enabled, String description) {
        new VcsTagTask()
                .description(description)
                .enabled(enabled)
                .defaultRepository()
                .build();
    }
}
