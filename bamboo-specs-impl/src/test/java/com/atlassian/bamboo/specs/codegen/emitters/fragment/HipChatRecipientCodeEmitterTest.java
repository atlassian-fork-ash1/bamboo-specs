package com.atlassian.bamboo.specs.codegen.emitters.fragment;

import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.notification.HipChatRecipient;
import com.atlassian.bamboo.specs.codegen.BambooSpecsGenerator;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class HipChatRecipientCodeEmitterTest {

    private static final String SECRET_API_TOKEN = "SECRET API TOKEN";

    @Test
    public void testEmitCode() throws Exception {
        HipChatRecipient room = new HipChatRecipient()
                .room("room")
                .apiToken(SECRET_API_TOKEN)
                .notifyUsers(true);
        BambooSpecsGenerator generator = new BambooSpecsGenerator(EntityPropertiesBuilders.build(room));

        String result = generator.emitCode();

        assertFalse("Emitted code shouldn't leak secrets.", result.contains(SECRET_API_TOKEN));
    }


}