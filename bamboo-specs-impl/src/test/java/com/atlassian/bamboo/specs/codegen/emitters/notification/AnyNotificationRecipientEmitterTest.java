package com.atlassian.bamboo.specs.codegen.emitters.notification;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.notification.AnyNotificationRecipient;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.notification.AnyNotificationRecipientProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.notification.CommittersRecipient;
import com.atlassian.bamboo.specs.builders.notification.ResponsibleRecipient;
import com.atlassian.bamboo.specs.builders.notification.WatchersRecipient;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class AnyNotificationRecipientEmitterTest {

    @Parameters(method = "emitCodeParameters")
    @Test
    public void testEmitCode(AnyNotificationRecipientProperties properties, String expectedCode) throws Exception {
        String result = new AnyNotificationRecipientEmitter().emitCode(new CodeGenerationContext(), properties);

        assertEquals(expectedCode, result);
    }


    private Object[][] emitCodeParameters() {
        return new Object[][]{
                {buildProperties(new WatchersRecipient()), "new WatchersRecipient()"},
                {buildProperties(new CommittersRecipient()), "new CommittersRecipient()"},
                {buildProperties(new ResponsibleRecipient()), "new ResponsibleRecipient()"},
                {buildProperties(new AnyNotificationRecipient(new AtlassianModule("com.atlassian.bamboo.plugin.system.notifications:recipient.watcher"))),
                        "new WatchersRecipient()"},
                {buildProperties(new AnyNotificationRecipient(new AtlassianModule("o:ther"))),
                        "new AnyNotificationRecipient(new AtlassianModule(\"o:ther\"))"},
        };
    }

    @NotNull
    private <T extends AnyNotificationRecipientProperties> AnyNotificationRecipientProperties buildProperties(NotificationRecipient<?, T> builder) {
        return EntityPropertiesBuilders.build(builder);
    }

}