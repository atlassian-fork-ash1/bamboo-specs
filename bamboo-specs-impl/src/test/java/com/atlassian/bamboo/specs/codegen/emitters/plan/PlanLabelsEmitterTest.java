package com.atlassian.bamboo.specs.codegen.emitters.plan;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.label.EmptyLabelsListProperties;
import com.atlassian.bamboo.specs.api.model.label.LabelProperties;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PlanLabelsEmitterTest {
    private CodeGenerationContext context;
    private PlanLabelsEmitter emitter;

    @Before
    public void setUp() {
        context = new CodeGenerationContext();
        emitter = new PlanLabelsEmitter();
    }

    @Test
    public void testEmitLabels() throws Exception {
        final String code = new PlanLabelsEmitter().emitCode(context, Stream.of("foo", "bar", "baz")
                .map(LabelProperties::new)
                .collect(Collectors.toList()));
        assertThat(code, is(".labels(\"foo\",\n"
                + "    \"bar\",\n"
                + "    \"baz\")"));
    }

    @Test
    public void testEmitEmptyLabels() throws Exception {
        final String code = new PlanLabelsEmitter().emitCode(context, Collections.singletonList(new EmptyLabelsListProperties()));
        assertThat(code, is(".noLabels()"));
    }
}
