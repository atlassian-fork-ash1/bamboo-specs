package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.DockerPullImageTask;
import com.atlassian.bamboo.specs.model.task.docker.DockerRegistryTaskProperties;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;


public class DockerRegistryEmitterTest {

    private DockerRegistryEmitter emitter;

    @Before
    public void setUp() {
        emitter = new DockerRegistryEmitter();
    }

    @Test
    public void emitUsernamePasswordAuthenticationCode() {

        DockerRegistryTaskProperties properties = EntityPropertiesBuilders.build(
                new DockerPullImageTask().dockerHubImage("image").authentication("username\"WithQuotes\"", "password\"WithQuotes\"", "email\"WithQuotes\""));

        String result = emitter.emitAuthenticationCode(properties, mock(CodeGenerationContext.class));

        assertFalse("Emitted code doesn't contain unescaped username", result.contains("username\"WithQuotes\""));
        assertFalse("Emitted code doesn't contain unescaped email", result.contains("email\"WithQuotes\""));

        assertTrue("Emitted code contains escaped username", result.contains("username\\\"WithQuotes\\\""));
        assertTrue("Emitted code contains escaped email", result.contains("email\\\"WithQuotes\\\""));

        assertFalse("Emitted code doesn't contain unescaped password", result.contains("password\"WithQuotes\""));
        assertTrue("Emitted code contains escaped password", result.contains("password\\\"WithQuotes\\\""));
    }

    @Test
    public void emitSharedCredentialsAuthenticationCode() {

        final String sharedCredentialsName = "mysharedcredentials";
        DockerRegistryTaskProperties properties = EntityPropertiesBuilders.build(
                new DockerPullImageTask()
                        .dockerHubImage("image")
                        .authentication(new SharedCredentialsIdentifier(sharedCredentialsName)));

        String result = emitter.emitAuthenticationCode(properties, mock(CodeGenerationContext.class));

        assertTrue("Emitted code should contain shared credentials name", result.contains(sharedCredentialsName));
    }
}