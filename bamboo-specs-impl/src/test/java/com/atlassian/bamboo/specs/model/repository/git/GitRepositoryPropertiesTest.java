package com.atlassian.bamboo.specs.model.repository.git;

import com.atlassian.bamboo.specs.api.builders.repository.PlanRepositoryLink;
import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.repository.PlanRepositoryLinkProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.builders.repository.git.UserPasswordAuthentication;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNull;

public class GitRepositoryPropertiesTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private final UserPasswordAuthentication authentication = new UserPasswordAuthentication("someUsername")
            .password("somePassword");

    private final String repositoryName = "someRepositoryName";
    private final String repositoryDescription = "someRepositoryDescription";
    private final String url = "https://bitbucket.org/atlassian/bamboo-docker-plugin.git";
    private final String branch = "someBranch";
    private final Duration timeout = Duration.ofMinutes(120);


    private GitRepository gitLinkedRepository;
    private PlanRepositoryLink gitPlanRepository;

    @Before
    public void setup() {
        gitLinkedRepository = new GitRepository()
                .name(repositoryName)
                .description(repositoryDescription)
                .url(url)
                .branch(branch)
                .authentication(authentication)
                .changeDetection(new VcsChangeDetection())
                .shallowClonesEnabled(true)
                .remoteAgentCacheEnabled(false)
                .submodulesEnabled(true)
                .commandTimeout(timeout)
                .verboseLogs(true)
                .fetchWholeRepository(false)
                .lfsEnabled(true);

        gitPlanRepository = PlanRepositoryLink.linkToGlobalRepository(gitLinkedRepository);
    }

    @Test
    public void testGitLinkedRepositoryPropertiesAreCreatedByBuilder() {
        final VcsRepositoryProperties repositoryProperties = EntityPropertiesBuilders.build(gitLinkedRepository);
        final GitRepositoryProperties gitRepositoryProperties = (GitRepositoryProperties) repositoryProperties;

        assertThat(gitRepositoryProperties.getName(), is(repositoryName));
        assertThat(gitRepositoryProperties.getDescription(), is(repositoryDescription));
        assertThat(gitRepositoryProperties.getUrl(), is(url));
        assertThat(gitRepositoryProperties.getBranch(), is(branch));
        assertThat(gitRepositoryProperties.getAuthenticationProperties(), is(EntityPropertiesBuilders.build(authentication)));
        assertThat(gitRepositoryProperties.isUseShallowClones(), is(true));
        assertThat(gitRepositoryProperties.isUseRemoteAgentCache(), is(false));
        assertThat(gitRepositoryProperties.isUseSubmodules(), is(true));
        assertThat(gitRepositoryProperties.getCommandTimeout(), is(timeout));
        assertThat(gitRepositoryProperties.isVerboseLogs(), is(true));
        assertThat(gitRepositoryProperties.isFetchWholeRepository(), is(false));
        assertThat(gitRepositoryProperties.isUseLfs(), is(true));
    }

    @Test
    public void testGitPlanRepositoryPropertiesAreCreatedByBuilder() {
        final PlanRepositoryLinkProperties repositoryProperties = EntityPropertiesBuilders.build(gitPlanRepository);
        final VcsRepositoryProperties gitRepositoryProperties = repositoryProperties.getRepositoryDefinition();

        assertThat(gitRepositoryProperties.getParentName(), is(repositoryName));
    }

    @Test
    public void testGitRepositoryAcceptsScpUrlFormat() {
        gitLinkedRepository.url("git@bitbucket.org:atlassian/bamboo-docker-plugin.git");
        EntityPropertiesBuilders.build(gitLinkedRepository);
    }

    @Test
    public void testGitRepositoryWithDefaultChangeDetection() {
        gitLinkedRepository.defaultChangeDetection();

        final VcsRepositoryProperties repositoryProperties = EntityPropertiesBuilders.build(gitLinkedRepository);
        final GitRepositoryProperties gitRepositoryProperties = (GitRepositoryProperties) repositoryProperties;

        assertNull(gitRepositoryProperties.getVcsChangeDetection());
    }

    @Test
    public void testGitLinkedRepositoryWithNoAuthentication() {
        gitLinkedRepository.withoutAuthentication();

        final VcsRepositoryProperties repositoryProperties = EntityPropertiesBuilders.build(gitLinkedRepository);
        final GitRepositoryProperties gitRepositoryProperties = (GitRepositoryProperties) repositoryProperties;

        assertNull(gitRepositoryProperties.getAuthenticationProperties());
    }

    @Test
    public void testGitLinkedRepositoryMustHaveName() {
        gitLinkedRepository.name(null);
        exception.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(gitLinkedRepository);
    }

    @Test
    public void testGitRepositoryMustUseSupportedUrlProtocol() {
        gitLinkedRepository.url("ftp://bitbucket.org/atlassian/bamboo-docker-plugin.git");
        exception.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(gitLinkedRepository);
    }

    @Test
    public void testGitRepositoryUrlCannotHaveInjectionCharacters() {
        gitLinkedRepository.url("https://bitbucket.org/<SCRIPT>alert(\"!!!\");</SCRIPT>atlassian/bamboo-docker-plugin.git");
        exception.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(gitLinkedRepository);
    }

    @Test
    public void testGitRepositoryBranchCannotHaveInjectionCharacters() {
        gitLinkedRepository.branch("<SCRIPT>alert(\"!!!\");</SCRIPT>");
        exception.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(gitLinkedRepository);
    }

    @Test
    public void testGitRepositoryChangeDetectionCannotHaveExtraConfiguration() {
        Map<String, Object> configuration = new HashMap<>();
        configuration.put("key", "value");

        final VcsChangeDetection changeDetection = new VcsChangeDetection()
                .configuration(configuration);
        gitLinkedRepository.changeDetection(changeDetection);
        exception.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(gitLinkedRepository);
    }
}
