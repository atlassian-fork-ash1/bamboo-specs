package com.atlassian.bamboo.specs.util;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LoggerTest {
    @Test
    public void testInfoLogging() {
        System.setProperty(Logger.LOG_LEVEL_PROPERTY, Logger.LogLevel.INFO.name());
        testLogging(true, false, false);
    }

    @Test
    public void testDebugLogging() {
        System.setProperty(Logger.LOG_LEVEL_PROPERTY, Logger.LogLevel.DEBUG.name());
        testLogging(true, true, false);
    }

    @Test
    public void testTraceLogging() {
        System.setProperty(Logger.LOG_LEVEL_PROPERTY, Logger.LogLevel.TRACE.name());
        testLogging(true, true, true);
    }

    private void testLogging(boolean shouldLogInfo, boolean shouldLogDebug, boolean shouldLogTrace) {
        testLogging(logger -> logger.info("%s %s", "foo", "bar"), shouldLogInfo);
        testLogging(logger -> logger.info(new RuntimeException()), shouldLogInfo);
        testLogging(logger -> logger.info(new RuntimeException(), "%s %s", "foo", "bar"), shouldLogInfo);
        testLogging(logger -> logger.log(Logger.LogLevel.INFO, "%s %s", "foo", "bar"), shouldLogInfo);
        testLogging(logger -> logger.log(Logger.LogLevel.INFO, new RuntimeException()), shouldLogInfo);
        testLogging(logger -> logger.log(Logger.LogLevel.INFO, new RuntimeException(), "%s %s", "foo", "bar"), shouldLogInfo);

        testLogging(logger -> logger.debug("%s %s", "foo", "bar"), shouldLogDebug);
        testLogging(logger -> logger.debug(new RuntimeException()), shouldLogDebug);
        testLogging(logger -> logger.debug(new RuntimeException(), "%s %s", "foo", "bar"), shouldLogDebug);
        testLogging(logger -> logger.log(Logger.LogLevel.DEBUG, "%s %s", "foo", "bar"), shouldLogDebug);
        testLogging(logger -> logger.log(Logger.LogLevel.DEBUG, new RuntimeException()), shouldLogDebug);
        testLogging(logger -> logger.log(Logger.LogLevel.DEBUG, new RuntimeException(), "%s %s", "foo", "bar"), shouldLogDebug);

        testLogging(logger -> logger.trace("%s %s", "foo", "bar"), shouldLogTrace);
        testLogging(logger -> logger.trace(new RuntimeException()), shouldLogTrace);
        testLogging(logger -> logger.trace(new RuntimeException(), "%s %s", "foo", "bar"), shouldLogTrace);
        testLogging(logger -> logger.log(Logger.LogLevel.TRACE, "%s %s", "foo", "bar"), shouldLogTrace);
        testLogging(logger -> logger.log(Logger.LogLevel.TRACE, new RuntimeException()), shouldLogTrace);
        testLogging(logger -> logger.log(Logger.LogLevel.TRACE, new RuntimeException(), "%s %s", "foo", "bar"), shouldLogTrace);
    }

    private void testLogging(Consumer<Logger> logCall, boolean shouldLogSomething) {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final PrintStream printStream = new PrintStream(outputStream);
        final Logger logger = new Logger(getClass(), printStream);

        logCall.accept(logger);

        final String loggedContent = new String(outputStream.toByteArray(), StandardCharsets.UTF_8);
        assertThat(StringUtils.isNotEmpty(loggedContent), is(shouldLogSomething));
    }
}
