package com.atlassian.bamboo.specs.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class TestFileUtils
{
    public static void createFile(File rootFolder, final String fileName, final String content) throws IOException
    {
        Files.write(getPathTo(rootFolder, fileName), content.getBytes(), StandardOpenOption.CREATE);
    }

    public static Path getPathTo(File rootFolder, final String fileName) {
        return Paths.get(rootFolder.getPath(), fileName);
    }
}
