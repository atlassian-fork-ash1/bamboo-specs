package com.atlassian.bamboo.specs;

import com.atlassian.bamboo.specs.api.BambooSpec;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@BambooSpec
public class TestHostileSpec {
    public static void main(String[] args) throws IOException {
        SpecsRunnerTest.specRunStartedTl.get().set(TestHostileSpec.class);
        try (final BufferedWriter bw = Files.newBufferedWriter(Paths.get("file"))) {
            bw.write("test");
            SpecsRunnerTest.specRunEndedTl.get().set(TestHostileSpec.class);
        }
    }
}
