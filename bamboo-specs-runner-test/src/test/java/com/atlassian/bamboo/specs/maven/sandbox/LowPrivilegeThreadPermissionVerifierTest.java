package com.atlassian.bamboo.specs.maven.sandbox;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FilePermission;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

class LowPrivilegeThreadPermissionVerifierTest {
    @Test
    void gettingCallerStackWorks() throws IOException {
        final Path tempDirectory = Files.createTempDirectory("permissionsTest");

        final Path readPath = Paths.get(tempDirectory.toString(), "/read");
        final Path writePath = Paths.get(tempDirectory.toString(), "/write");
        Path symlinkPath = null;
        try {

            Files.createDirectory(readPath);
            Files.createDirectory(writePath);
            symlinkPath = Files.createSymbolicLink(Paths.get(writePath.toString(), "/symlink"), Paths.get("../"));

            final LowPrivilegeThreadPermissionVerifier permissionVerifier =
                    new LowPrivilegeThreadPermissionVerifier(readPath, writePath);

            final FilePermission standardReadPermission = new FilePermission(readPath.toString(), "read");
            final FilePermission standardWritePermission = new FilePermission(writePath.toString(), "write");

            final FilePermission checkWriteForReadDirectory = new FilePermission(readPath.toString(), "write");
            final FilePermission writeParentPermission = new FilePermission(readPath.toString() + "/..", "write");
            final FilePermission writeSymlinkPermission = new FilePermission(symlinkPath.toString(), "write");

            Assertions.assertTrue(permissionVerifier.checkPermissionFor(standardReadPermission));
            Assertions.assertTrue(permissionVerifier.checkPermissionFor(standardWritePermission));

            Assertions.assertFalse(permissionVerifier.checkPermissionFor(checkWriteForReadDirectory));
            Assertions.assertFalse(permissionVerifier.checkPermissionFor(writeParentPermission));
            Assertions.assertFalse(permissionVerifier.checkPermissionFor(writeSymlinkPermission));
        } finally {
            if (symlinkPath != null) {
                Files.deleteIfExists(symlinkPath);
            }
            Files.deleteIfExists(writePath);
            Files.deleteIfExists(readPath);
            Files.deleteIfExists(tempDirectory);
        }

    }

    @Test
    void checkNullPermissionsDirectory() {
        final LowPrivilegeThreadPermissionVerifier permissionVerifier =
                new LowPrivilegeThreadPermissionVerifier(null, null);

        final FilePermission standardReadPermission = new FilePermission(".", "read");
        final FilePermission standardWritePermission = new FilePermission(".", "write");

        Assertions.assertFalse(permissionVerifier.checkPermissionFor(standardReadPermission));
        Assertions.assertFalse(permissionVerifier.checkPermissionFor(standardWritePermission));
    }

}
