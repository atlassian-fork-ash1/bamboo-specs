package com.atlassian.bamboo.specs.maven.sandbox;

import com.google.common.annotations.VisibleForTesting;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public final class PrivilegedThreads {
    private static volatile ThreadPermissionVerifier defaultPermissionVerfier;

    private static AtomicReference<Map<Thread, ThreadPermissionVerifier>> permissionVerifiers = new AtomicReference<>();

    private PrivilegedThreads() {
    }

    static ThreadPermissionVerifier getThreadPermissionChecker() {
        final ThreadPermissionVerifier threadPermissionVerifier = permissionVerifiers.get().get(Thread.currentThread());
        return threadPermissionVerifier!=null ? threadPermissionVerifier : defaultPermissionVerfier;
    }

    static void setThreadPermissionCheckers(final Map<Thread, ThreadPermissionVerifier> verifiers, final ThreadPermissionVerifier defaultPermissionVerifier) {
        if (!permissionVerifiers.compareAndSet(null, verifiers)) {
            throw new IllegalStateException("Cannot nest secure method invocations");
        }
        defaultPermissionVerfier = defaultPermissionVerifier;
    }

    @VisibleForTesting
    public static void resetThreadPermissionCheckers() {
        if (permissionVerifiers.get() != null) {
            getThreadPermissionChecker().checkPermission(new RuntimePermission("setSecurityManager"));
            defaultPermissionVerfier = null;
            permissionVerifiers.set(null);
        }
    }
}
