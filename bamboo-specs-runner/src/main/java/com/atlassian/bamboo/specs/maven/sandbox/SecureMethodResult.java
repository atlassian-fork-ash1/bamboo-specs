package com.atlassian.bamboo.specs.maven.sandbox;

public final class SecureMethodResult {
    private SecureMethodResult() {

    }

    public static final ThreadLocal<Object> RESULT = new ThreadLocal<>();
}
